import unittest
import sys
import ParserGenerator as PG

class TestParserGenerator(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_expand_blueprints(self):
        blueprint = \
"""
    def {name:0}({args:0}):
        {body:2}
"""
        table = {'name': 'foo', 'args': 'bar', 'body': 
'''print "Hello World!"
return 0'''}

        print PG.Utils.expandBlueprint(blueprint, table)

        pg = PG.ParserGenerator()


def runAllTests():
    # Load all tests from module
    alltests = unittest.TestLoader().loadTestsFromModule(sys.modules[__name__])

    unittest.TextTestRunner(verbosity=2).run(alltests)

if __name__ == '__main__':
    runAllTests()