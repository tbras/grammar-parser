
__author__ = "Tiago Bras - tiagobraspt@gmail.com"

'''
Regexes. 

What's implemented:

 - Quantifiers:

+ : one or more
* : zero or more
? : one or none

- Parenthesis

- Character classes:

\w : a-zA-Z_
\W : everything but a-zA-Z_
\s : [ \t\r\n\v\f]
\S : everything but [ \t\r\n\v\f]
\d : [0-9]
\D : everything but [0-9]
. : everything but newline

- Ranges/groups [...] or [^...]

What's NOT implemented:

{ } quantifies.

Non-greediness/greediness.

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

Lexer usage example:

lexer = JL.JBLexer()
lexer.addRule("PATTERN", "\"[^\"]+\"", 1)
lexer.addRule("WORD", "\w+", 2)

token = lexer.nextToken()

while token != None:
    print str(token)

    token = lexer.nextToken()
'''

import JamesLexerTests as JLT
import re

def main():
    JLT.runAllTests()
        
class JBLexer(object):
    def __init__(self, defaultStartCondition=0):
        super(JBLexer, self).__init__()
        self.definitions = []
        self.rules = []
        self.defaultStartCondition = defaultStartCondition
        self.currStartCondition = self.defaultStartCondition
        self.currIndex = -1
        self.string = ""

    def __repr__(self):
        return "JBLexer({})".format(self.defaultStartCondition)

    def initTokenizer(self, string):
        self.currIndex = 0
        self.string = string
        self.currStartCondition = self.defaultStartCondition

    def addDefinition(self, name, definition):
        d = JBLexerDefinition(name, self.expandString(definition))

        self.definitions.append(d)

    def addRule(self, name, pattern, tokenCode, startConditions=None, setCondition=None, action=None):
        pattern = self.expandString(pattern)

        if startConditions == None:
            startConditions = [self.defaultStartCondition]

        self.rules.append(JBLexerRule(name, pattern, tokenCode, startConditions, setCondition, action))

    # Expand string definitions, \w{string}{pattern} => \w\w+\"[^"]"
    def expandString(self, string):
        expansions = re.findall("\{\w+\}", string)

        for expansion in expansions:
            name = expansion[1:-1]

            for d in self.definitions:
                if d.name == name:
                    string = re.sub("{" + name + "}", d.definition, string)

        # Check if all definitions were expanded
        m = re.findall("\{\w+\}", string)
        if len(m) > 0:
            raise Exception("Could not expand all definitions from string: '{}'. Make sure '{}' are defined.".format(string, ", ".join(m)))

        return string

    def nextToken(self):
        if (self.currIndex < 0) or (len(self.string) == 0):
            raise Exception("Tokenizer is not initiated.")

        if (self.currIndex >= len(self.string)):
            return None
        
        token = None

        # Only check for rules that has the current start condition
        validRules = [rule for rule in self.rules if (self.currStartCondition in rule.startConditions)]

        for r in validRules:
            success, nextIndex = r.match(self.string, self.currIndex)

            if success:
                if r.action.code == JBLexerAction.ActionIgnoreToken:
                    self.currIndex = nextIndex

                    if r.setCondition != None:
                        self.currStartCondition = r.setCondition

                    token = self.nextToken()
                else:
                    if r.action.code == JBLexerAction.ActionSaveText:
                        token = JBLexerToken(r.name, r.tokenCode, self.string[self.currIndex:nextIndex])
                    elif r.action.code == JBLexerAction.ActionSaveSubText:
                        maxStringIndex = nextIndex - self.currIndex
    
                        # Validade sub string indexes
                        if (r.action.args != None) and (len(r.action.args) == 2):
                            fromIndex = r.action.args[0]
                            toIndex = r.action.args[1]
                            if fromIndex < 0:
                                fromIndex += maxStringIndex + 1
                            if toIndex < 0:
                                toIndex += maxStringIndex + 1
    
                            if (fromIndex < toIndex) and (toIndex <= maxStringIndex):
                                token = JBLexerToken(r.name, r.tokenCode, self.string[self.currIndex+fromIndex:self.currIndex+toIndex])
                        else:
                            raise Exception("Invalid indexes for sub string.")
                    else:
                        token = JBLexerToken(r.name, r.tokenCode)
    
                    self.currIndex = nextIndex
    
                    if r.setCondition != None:
                        self.currStartCondition = r.setCondition
    
                break

        if token == None:
            raise Exception("Parsing error at index = {}, char = '{}'.".format(self.currIndex, self.string[self.currIndex]))

        return token

    def tokenizeString(self, string):
        stringLen = len(string)
        tokens = []

        i = 0
        startCondition = self.defaultStartCondition
        while i < stringLen:
            atLeastOneMatch = False

            # Only check for rules that has the current start condition
            validRules = [rule for rule in self.rules if (startCondition in rule.startConditions)]

            for r in validRules:
                success, nextIndex = r.match(string, i)

                if success:
                    if r.action.code == JBLexerAction.ActionIgnoreToken:
                        # Don't add token
                        pass
                    elif r.action.code == JBLexerAction.ActionSaveText:
                        tokens.append(JBLexerToken(r.name, r.tokenCode, string[i:nextIndex]))
                    elif r.action.code == JBLexerAction.ActionSaveSubText:
                        maxStringIndex = nextIndex - i

                        # Validade sub string indexes
                        if (r.action.args != None) and (len(r.action.args) == 2):
                            fromIndex = r.action.args[0]
                            toIndex = r.action.args[1]
                            if fromIndex < 0:
                                fromIndex += maxStringIndex + 1
                            if toIndex < 0:
                                toIndex += maxStringIndex + 1

                            if (fromIndex < toIndex) and (toIndex <= maxStringIndex):
                                tokens.append(JBLexerToken(r.name, r.tokenCode, string[i+fromIndex:i+toIndex]))
                    else:
                        tokens.append(JBLexerToken(r.name, r.tokenCode))

                    i = nextIndex
                    atLeastOneMatch = True

                    if r.setCondition != None:
                        startCondition = r.setCondition

                    break            

            if atLeastOneMatch == False:
                print "Parsing error at char:", string[i]
                break

        return tokens

    def getLexerTokensDict(self):
        tokensDict = {}

        for rule in self.rules:
            tokensDict[rule.name] = rule.tokenCode

        return tokensDict


class JBLexerToken(object):
    def __init__(self, name, code, var=None):
        super(JBLexerToken, self).__init__()
        self.name = name
        self.code = code
        self.var = var

    def __str__(self):
        return "({}, {}, '{}')".format(self.name, self.code, self.var)

    def __repr__(self):
        return "JBLexerToken({}, {}, {})".format(self.name, self.code, self.var)

        
class JBLexerDefinition(object):
    def __init__(self, name, definition):
        super(JBLexerDefinition, self).__init__()
        self.name = name
        self.definition = definition

    def __repr__(self):
        return "JBLexerDefinition({}, {})".format(self.name, self.definition)

    # def expandDefinitions(self, definitionsList):
    #     expansions = re.findall("\{\w+\}", self.definition)

    #     for expansion in expansions:
    #         name = expansion[1:-1]

    #         for d in definitionsList:
    #             if d.name == name:
    #                 self.definition = re.sub("{" + name + "}", d.definition, self.definition)

class JBLexerAction(object):
    ActionToken, ActionIgnoreToken, ActionSaveText, ActionSaveSubText = (0, 1, 2, 3)

    def __init__(self, code, args=None):
        super(JBLexerAction, self).__init__()
        self.code = code
        self.args = args

    def __repr__(self):
        return "JBLexerAction({}, {})".format(self.code, self.args)
        
 
class JBLexerRule(object):
    def __init__(self, name, pattern, tokenCode, startConditions=None, setCondition=None, action=None):
        super(JBLexerRule, self).__init__()
        self.name = name
        self.pattern = pattern
        self.tokenCode = tokenCode
        self.startConditions = startConditions
        self.setCondition = setCondition
        self.action = action
        self.nfa = self.generateNFA(self.pattern)

        if self.action == None:
            self.action = JBLexerAction(JBLexerAction.ActionToken)

        if self.startConditions == None:
            self.startConditions = []

    def __repr__(self):
        return "JBLexerRule({}, {}, {}, {}, {}, {})".format(
            self.name, self.pattern, self.tokenCode, self.startConditions, self.setCondition, self.action)

    def __str__(self):
        return "({}, {} [{}-{}])".format(self.name, self.pattern, [str(s) for s in self.startConditions], self.setCondition)

    def getNFA(self):
        return self.nfa

    def generateNFA(self, pattern):
        nfa = self.re2nfa(pattern)

        if nfa == None:
            raise Exception("Could not compile Pattern to NFA.")

        return nfa

    # Given: [a-d] => [abcd], a{2,4} => aa|aaa|aaaa, (ab){3, } = (ab)(ab)(ab)+
    def expandPattern(self, pattern):
        pass

    def re2nfa(self, regex, rootState=None, finalState=None):
        if rootState == None:
            rootState = JBState(isStartState=True)

        if finalState == None:
            finalState = JBState(isFinalState=True)

        lastState = rootState

        regexLen = len(regex)
        i = 0
        stop = 0
        while i < regexLen:
            stop += 1
            if stop > 20:
                raise Exception("Something went terrible wrong!!!")

            c = regex[i]

            if c == '|':
                lastState.addTransition(JBTransition(JBTransitionToken(EPSILON), finalState))

                lastState = rootState
            else:
                s0 = JBState()
                s1 = JBState()

                if c == '(':
                    pLevel = 1
                    charsBuffer = []
                    nextLastState = JBState()
        
                    i += 1
                    while (pLevel > 0) and (i < regexLen):
                        tmpC = regex[i]
        
                        if tmpC == '(':
                            pLevel += 1
                        elif tmpC == ')':
                            pLevel -= 1
        
                            if pLevel == 0:
                                break
                        else:
                            charsBuffer.append(tmpC)
        
                        i += 1
        
        
                    self.re2nfa("".join(charsBuffer), s0, s1)
                    charsBuffer = []
                elif c == '\\' and (i < regexLen - 1):
                    i += 1
                    c = regex[i]

                    if c in 'wWdDsS':
                        if c == 'w':
                            s0.addTransition(JBTransition(JBTransitionToken(ALPHANUMERIC), s1))
                        elif c == 'W':
                            s0.addTransition(JBTransition(JBTransitionToken(NON_ALPHANUMERIC), s1))
                        elif c == 'd':
                            s0.addTransition(JBTransition(JBTransitionToken(DIGIT), s1))
                        elif c == 'D':
                            s0.addTransition(JBTransition(JBTransitionToken(NON_DIGIT), s1))
                        elif c == 's':
                            s0.addTransition(JBTransition(JBTransitionToken(WHITESPACE), s1))
                        elif c == 'S':
                            s0.addTransition(JBTransition(JBTransitionToken(NON_WHITESPACE), s1))
                    else:
                        s0.addTransition(JBTransition(JBTransitionToken(CHAR, c), s1))
                elif c == '[':
                    allExpect = False

                    if regex[i+1] == '^':
                        allExpect = True
                        i += 1

                    cBuffer = []

                    i += 1
                    while regex[i] != ']':
                        # Escaped Char
                        if (regex[i] == '\\') and (i < regexLen - 2):
                            i += 1
                            cBuffer.append(regex[i])
                        else:
                            R_LOWER_CHARS = "abcdefghijklmnopqrstuvwxyz"
                            R_UPPER_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                            R_DIGITS = "0123456789"

                            # Given [d-g] => defg
                            if (regex[i] in R_LOWER_CHARS) and (regex[i+1] == '-') and (regex[i+2] in R_LOWER_CHARS):
                                fromChar = regex[i]
                                toChar = regex[i+2]

                                if fromChar < toChar:
                                    cBuffer.extend([rc for rc in R_LOWER_CHARS if rc >= fromChar and rc <= toChar])
                                else:
                                    raise Exception("Range is invalid (it's reversed): [{}-{}]".format(fromChar, toChar))

                                i += 2
                            elif (regex[i] in R_UPPER_CHARS) and (regex[i+1] == '-') and (regex[i+2] in R_UPPER_CHARS):
                                fromChar = regex[i]
                                toChar = regex[i+2]

                                if fromChar < toChar:
                                    cBuffer.extend([rc for rc in R_UPPER_CHARS if rc >= fromChar and rc <= toChar])
                                else:
                                    raise Exception("Range is invalid (it's reversed): [{}-{}]".format(fromChar, toChar))

                                i += 2
                            elif (regex[i] in R_DIGITS) and (regex[i+1] == '-') and (regex[i+2] in R_DIGITS):
                                fromChar = regex[i]
                                toChar = regex[i+2]

                                if fromChar < toChar:
                                    cBuffer.extend([rc for rc in R_DIGITS if (rc >= fromChar) and (rc <= toChar)])
                                else:
                                    raise Exception("Range is invalid (it's reversed): [{}-{}]".format(fromChar, toChar))

                                i += 2
                            else:
                                cBuffer.append(regex[i])

                        i += 1

                    if allExpect:
                        s0.addTransition(JBTransition(JBTransitionToken(NON_RANGE, "".join(cBuffer)), s1))
                    else:
                        s0.addTransition(JBTransition(JBTransitionToken(RANGE, "".join(cBuffer)), s1))
                elif c == '.':
                    s0.addTransition(JBTransition(JBTransitionToken(ANY_CHAR_BUT_NEWLINE), s1))
                else:
                    s0.addTransition(JBTransition(JBTransitionToken(CHAR, c), s1))
        
                # Check if there's a modifier
                if (i + 1 < regexLen) and (regex[i+1] in '?+*'):
                    nextC = regex[i+1] 
        
                    if nextC == '?':
                        s2f = JBState()
        
                        lastState.addTransition(JBTransition(JBTransitionToken(EPSILON), s0))
                        lastState.addTransition(JBTransition(JBTransitionToken(EPSILON), s2f))
                        
                        s1.addTransition(JBTransition(JBTransitionToken(EPSILON), s2f))
        
                        lastState = s2f
                        i += 1
                    elif nextC == '*':
                        s2f = JBState()
        
                        lastState.addTransition(JBTransition(JBTransitionToken(EPSILON), s0))
                        lastState.addTransition(JBTransition(JBTransitionToken(EPSILON), s2f))
        
                        s1.addTransition(JBTransition(JBTransitionToken(EPSILON), s0))
                        s1.addTransition(JBTransition(JBTransitionToken(EPSILON), s2f))
        
                        lastState = s2f
                        i += 1
                    elif nextC == '+':
                        s2f = JBState()
        
                        lastState.addTransition(JBTransition(JBTransitionToken(EPSILON), s0))
        
                        s1.addTransition(JBTransition(JBTransitionToken(EPSILON), s0))
                        s1.addTransition(JBTransition(JBTransitionToken(EPSILON), s2f))
        
                        lastState = s2f
                        i += 1
                else:
                    lastState.addTransition(JBTransition(JBTransitionToken(EPSILON), s0))
        
                    lastState = s1

            i += 1

        lastState.addTransition(JBTransition(JBTransitionToken(EPSILON), finalState))

        return rootState

    def match(self, string, startIndex=0):
        nfa = self.getNFA()

        nfa.inputIndex = startIndex

        stack = JBStack()
        stack.push(nfa)

        stop = 0

        lastValidState = nfa
        while (stack.isEmpty() == False):
            currState = stack.pop()

            i = currState.inputIndex

            if i >= len(string):
                break

            c = string[i]

            epsilonList = []
            nonEpsilonListChar = []
            nonEpsilonListCharClass = []
            for t in reversed(currState.transitions):
                if t.isMatch(c):
                    s = t.nextState

                    if t.tokenMatch.code == EPSILON:
                        s.inputIndex = i
                        epsilonList.append(s)
                    else:
                        s.inputIndex = i + 1

                        if t.tokenMatch.code == CHAR:
                            nonEpsilonListChar.append(s)
                        else:
                            nonEpsilonListCharClass.append(s)

                        lastValidState = s

            for s in nonEpsilonListCharClass:
                stack.push(s)
            for s in nonEpsilonListChar:
                stack.push(s)
            for s in epsilonList:
                stack.push(s)

            if stop > 500:
                print "This should not have happened!!!"
                break

            stop += 1

        # Reach a accepting state only by epsilons
        stack = JBStack()
        stack.push(lastValidState)
        success = False
        out = 0
        while stack.isNotEmpty():
            currState = stack.pop()

            if currState.isFinalState:
                success = True
                break

            for t in reversed(currState.transitions):
                if t.tokenMatch.code == EPSILON:
                    stack.push(t.nextState)

            if out > 50:
                print "This should not have happened!!!"
                break

            out += 1

        return [success, lastValidState.inputIndex]

# Token codes
CHAR = 0
EPSILON = 1
ALPHANUMERIC = 2
NON_ALPHANUMERIC = 3
DIGIT = 4
NON_DIGIT = 5
WHITESPACE = 6
NON_WHITESPACE = 7
RANGE = 8
NON_RANGE = 9
ANY_CHAR_BUT_NEWLINE = 10

gTokenStringValue = {
    CHAR: 'CHAR', 
    EPSILON: 'EPSILON', 
    ALPHANUMERIC: 'ALPHANUMERIC',
    NON_ALPHANUMERIC: 'NON_ALPHANUMERIC', 
    DIGIT: 'DIGIT', 
    NON_DIGIT: 'NON_DIGIT',
    WHITESPACE: 'WHITESPACE', 
    NON_WHITESPACE: 'NON_WHITESPACE', 
    RANGE: 'RANGE',
    NON_RANGE: 'NON_RANGE', 
    ANY_CHAR_BUT_NEWLINE: 'ANY_CHAR_BUT_NEWLINE'
}

class JBTransition(object):
    CHARS_ALPHANUMERIC = list("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_")
    CHARS_DIGIT = list("0123456789")
    CHARS_WHITESPACE = list(" \t\r\n\v\f")

    def __init__(self, tokenMatch, nextState):
        super(JBTransition, self).__init__()
        self.tokenMatch = tokenMatch
        self.nextState = nextState

    def __repr__(self):
        return "JBTransition({}, {})".format(self.tokenMatch, self.nextState)

    def __str__(self):
        return "({}, {})".format(str(self.tokenMatch), str(self.nextState.code))

    def isMatch(self, theInput):
        if self.tokenMatch.code == ALPHANUMERIC:
            if (theInput in JBTransition.CHARS_ALPHANUMERIC):
                return True
        elif self.tokenMatch.code == NON_ALPHANUMERIC:
            if (theInput in JBTransition.CHARS_ALPHANUMERIC) == False:
                return True
        elif self.tokenMatch.code == DIGIT:
            if (theInput in JBTransition.CHARS_DIGIT):
                return True
        elif self.tokenMatch.code == NON_DIGIT:
            if (theInput in JBTransition.CHARS_DIGIT) == False:
                return True
        elif self.tokenMatch.code == WHITESPACE:
            if (theInput in JBTransition.CHARS_WHITESPACE):
                return True
        elif self.tokenMatch.code == NON_WHITESPACE:
            if (theInput in JBTransition.CHARS_WHITESPACE) == False:
                return True
        elif self.tokenMatch.code == RANGE:
            if (theInput in self.tokenMatch.var):
                return True
        elif self.tokenMatch.code == NON_RANGE:
            if (theInput in self.tokenMatch.var) == False:
                return True
        elif self.tokenMatch.code == CHAR:
            if theInput == self.tokenMatch.var:
                return True
        elif self.tokenMatch.code == EPSILON:
            return True
        elif self.tokenMatch.code == ANY_CHAR_BUT_NEWLINE:
            if (theInput in "\n\r") == False:
                return True

        return False        

class JBState(object):
    currCode = 0

    def __init__(self, code=None, transitions=None, isStartState=False, isFinalState=False, inputIndex=0):
        super(JBState, self).__init__()
        self.code = code

        if self.code == None:
            self.code = JBState.currCode

            JBState.currCode += 1

        self.transitions = transitions

        if self.transitions == None:
            self.transitions = []

        self.isStartState = isStartState
        self.isFinalState = isFinalState
        self.inputIndex = inputIndex
        self.visited = False


    def addTransition(self, transition):
        self.transitions.append(transition)

    def __str__(self):
        trans = "[" + ", ".join([str(t) for t in self.transitions]) + "]"

        return "({}, {}, {}, {}, {})".format(self.code, trans, self.isStartState, self.isFinalState, self.inputIndex)

    def __repr__(self):
        return "{}({}, {}, {}, {}, {})".format(type(self).__name__, self.code, self.transitions, 
            self.isStartState, self.isFinalState, self.inputIndex)

class JBTransitionToken(object):
    def __init__(self, code, var=None):
        super(JBTransitionToken, self).__init__()
        self.code = code
        self.var = var

    def __repr__(self):
        return "JBTransitionToken({}, {})".format(self.code, repr(self.var))

    def __str__(self):
        return "({}, {})".format(gTokenStringValue[self.code], repr(self.var))

    def getCode(self):
        return self.code

    def getVar(self):
        return self.var

class JBStack: 
    def __init__(self):
        self.items = [] 

    def top(self):
        return self.items[-1]

    def push(self, item):
        self.items.append(item) 

    def pop(self):
        return self.items.pop() 

    def isEmpty(self):
        return (self.items == [])

    def isNotEmpty(self):
        return (self.items != [])

    def __str__(self):
        return "[" + " ".join([str(s) for s in reversed(self.items)]) + "]"

# def getStateForNextCharMatch(char, state):
#     stack = JBStack()
#     stack.push(state)

#     out = 0
#     while stack.isNotEmpty():
#         currState = stack.pop()

#         for t in reversed(currState.transitions):
#             if t.tokenMatch.code == EPSILON:
#                 stack.push(t.nextState)
#             elif (t.tokenMatch.code == CHAR) and (t.tokenMatch.var == char):
#                 return t.nextState


#         if out > 50:
#             print "This should not have happened!!!"
#             break

#         out += 1

#     return None

# def printStateTree(state):
#     if state == None:
#         return

#     stack = JBStack()

#     stack.push(state)

#     visitedCodes = []

#     buf = []

#     while stack.isEmpty() == False:
#         s = stack.pop()

#         visitedCodes.append(s.code)

#         for t in s.transitions:
#             buf.append("(" + str(t) + " " + str(s) + " - " + str(t.nextState) + ")")

#             if (t.nextState.code in visitedCodes) == False:
#                 stack.push(t.nextState)

#     print " ".join(buf)


# def nfaReduceEpsilons(state):
#     while True:
#         epsilonsReducedCount = 0

#         stack = JBStack()
#         stack.push(state)
    
#         visitedCodes = []
    
#         while stack.isEmpty() == False:
#             s = stack.pop()
    
#             visitedCodes.append(s.code)
    
#             for t in s.transitions:
#                 if (t.nextState.code in visitedCodes) == False:
#                     stack.push(t.nextState)
    
#             if (len(s.transitions) == 1) \
#                     and (s.transitions[0].tokenMatch.code == EPSILON) \
#                     and (s.transitions[0].nextState.isStartState == False):
#                 s.isFinalState = s.transitions[0].nextState.isFinalState
#                 s.transitions = s.transitions[0].nextState.transitions
    
#                 epsilonsReducedCount += 1

#         if epsilonsReducedCount == 0:
#             break

#     return state

if __name__ == '__main__':
    main()
