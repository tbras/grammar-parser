import pprint as pp

class JBParserToken(object):
    JB_EOS = 0
    LESS = 1
    PATTERN = 2
    R_BRACKET = 3
    OPT_PARSER_NAME = 4
    GRAMMAR_HEADER = 5
    SEMI_COLON = 6
    STRING = 7
    TOKEN_ACTION = 8
    BEGIN = 9
    RP = 10
    OPT_OUTPUT_LANGUAGE = 11
    PIPE = 12
    COLON = 13
    LP = 14
    PROD_ACTION_BEGIN = 15
    L_BRACKET = 16
    END_STATEMENT = 17
    PROD_ACTION_END = 18
    TOKEN = 19
    DEFINITIONS_HEADER = 20
    WHITESPACE = 21
    GREATER = 22
    EPSILON = 23
    RULES_HEADER = 24
    COMMA = 25
    parser = 26
    optional_section = 27
    optional_args = 28
    optional_args_more = 29
    optional_arg = 30
    definitions_section = 31
    d_statements = 32
    d_stmt = 33
    rules_section = 34
    r_statements = 35
    r_stmt = 36
    r_start_conditions = 37
    r_start_conditions_more = 38
    r_actions = 39
    r_action_more = 40
    r_action = 41
    r_token_more = 42
    grammar_section = 43
    g_productions = 44
    g_production = 45
    g_right = 46
    g_right_more = 47
    g_strings = 48
    g_strings_more = 49

    def __init__(self, code, var=None):
        super(JBParserToken, self).__init__()
        self.code = code
        self.var = var

    def __repr__(self):
        return "{}({}, {})".format(type(self).__name__, self.code, self.var)

    def __str__(self):
        stringValue = {
            0: 'JB_EOS',
            1: 'LESS',
            2: 'PATTERN',
            3: 'R_BRACKET',
            4: 'OPT_PARSER_NAME',
            5: 'GRAMMAR_HEADER',
            6: 'SEMI_COLON',
            7: 'STRING',
            8: 'TOKEN_ACTION',
            9: 'BEGIN',
            10: 'RP',
            11: 'OPT_OUTPUT_LANGUAGE',
            12: 'PIPE',
            13: 'COLON',
            14: 'LP',
            15: 'PROD_ACTION_BEGIN',
            16: 'L_BRACKET',
            17: 'END_STATEMENT',
            18: 'PROD_ACTION_END',
            19: 'TOKEN',
            20: 'DEFINITIONS_HEADER',
            21: 'WHITESPACE',
            22: 'GREATER',
            23: 'EPSILON',
            24: 'RULES_HEADER',
            25: 'COMMA',
            26: 'parser',
            27: 'optional_section',
            28: 'optional_args',
            29: 'optional_args_more',
            30: 'optional_arg',
            31: 'definitions_section',
            32: 'd_statements',
            33: 'd_stmt',
            34: 'rules_section',
            35: 'r_statements',
            36: 'r_stmt',
            37: 'r_start_conditions',
            38: 'r_start_conditions_more',
            39: 'r_actions',
            40: 'r_action_more',
            41: 'r_action',
            42: 'r_token_more',
            43: 'grammar_section',
            44: 'g_productions',
            45: 'g_production',
            46: 'g_right',
            47: 'g_right_more',
            48: 'g_strings',
            49: 'g_strings_more'
        }

        return "({}, {})".format(stringValue[self.code], "'{}'".format(self.var) if self.var != None else None)

class JBStack: 
    def __init__(self, items=None):
        self.items = items

        if self.items == None:
            self.items = [] 

    def top(self):
        return self.items[-1]

    def push(self, item):
        self.items.append(item) 

    def pop(self):
        return self.items.pop() 

    def isEmpty(self):
        return (self.items == [])

    def isNotEmpty(self):
        return (self.items != [])

    def __repr__(self):
        return "{}({})".format(type(self).__name__, self.items)

class JBLexerToken(object):
    # Tokens
    CHAR = 0
    EPSILON = 1
    ALPHANUMERIC = 2
    NON_ALPHANUMERIC = 3
    DIGIT = 4
    NON_DIGIT = 5
    WHITESPACE = 6
    NON_WHITESPACE = 7
    RANGE = 8
    NON_RANGE = 9
    ANY_CHAR_BUT_NEWLINE = 10

    def __init__(self, code, var=None):
        super(JBLexerToken, self).__init__()
        self.code = code
        self.var = var

    def __repr__(self):
        return "{}({}, {})".format(type(self).__name__, self.code, self.var)

    # def __str__(self):
    #     return "({}, '{}')".format(JBLexerToken.tokenStringValue(self.code), self.var)

    @classmethod
    def tokenStringValue(cls, tokenCode):
        # Token string value
        tokenStringValue = {
            JBLexerToken.CHAR: 'CHAR', 
            JBLexerToken.EPSILON: 'EPSILON', 
            JBLexerToken.ALPHANUMERIC: 'ALPHANUMERIC',
            JBLexerToken.NON_ALPHANUMERIC: 'NON_ALPHANUMERIC', 
            JBLexerToken.DIGIT: 'DIGIT', 
            JBLexerToken.NON_DIGIT: 'NON_DIGIT',
            JBLexerToken.WHITESPACE: 'WHITESPACE', 
            JBLexerToken.NON_WHITESPACE: 'NON_WHITESPACE', 
            JBLexerToken.RANGE: 'RANGE',
            JBLexerToken.NON_RANGE: 'NON_RANGE', 
            JBLexerToken.ANY_CHAR_BUT_NEWLINE: 'ANY_CHAR_BUT_NEWLINE'
        }

        return tokenStringValue[tokenCode]

class JBLexerTransition(object):
    # Characters sets
    CHARS_ALPHANUMERIC = list("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_")
    CHARS_DIGIT = list("0123456789")
    CHARS_WHITESPACE = list(" \t\r\n\v\f")

    def __init__(self, arg):
        super(JBLexerTransition, self).__init__()
        self.arg = arg

    def __init__(self, lexerToken, nextState):
        super(JBLexerTransition, self).__init__()
        # Token that allows going to the next state
        self.lexerToken = lexerToken
        self.nextState = nextState

    def __repr__(self):
        return "{}({}, {})".format(type(self).__name__, self.lexerToken, self.nextState)

    def __str__(self):
        return "({}, {})".format(str(self.lexerToken), str(self.nextState.code))

    def isMatch(self, theInput):
        if self.lexerToken.code == JBLexerToken.ALPHANUMERIC:
            if (theInput in JBLexerTransition.CHARS_ALPHANUMERIC):
                return True
        elif self.lexerToken.code == JBLexerToken.NON_ALPHANUMERIC:
            if (theInput in JBLexerTransition.CHARS_ALPHANUMERIC) == False:
                return True
        elif self.lexerToken.code == JBLexerToken.DIGIT:
            if (theInput in JBLexerTransition.CHARS_DIGIT):
                return True
        elif self.lexerToken.code == JBLexerToken.NON_DIGIT:
            if (theInput in JBLexerTransition.CHARS_DIGIT) == False:
                return True
        elif self.lexerToken.code == JBLexerToken.WHITESPACE:
            if (theInput in JBLexerTransition.CHARS_WHITESPACE):
                return True
        elif self.lexerToken.code == JBLexerToken.NON_WHITESPACE:
            if (theInput in JBLexerTransition.CHARS_WHITESPACE) == False:
                return True
        elif self.lexerToken.code == JBLexerToken.RANGE:
            if (theInput in self.lexerToken.var):
                return True
        elif self.lexerToken.code == JBLexerToken.NON_RANGE:
            if (theInput in self.lexerToken.var) == False:
                return True
        elif self.lexerToken.code == JBLexerToken.CHAR:
            if theInput == self.lexerToken.var:
                return True
        elif self.lexerToken.code == JBLexerToken.EPSILON:
            return True
        elif self.lexerToken.code == JBLexerToken.ANY_CHAR_BUT_NEWLINE:
            if (theInput in "\n\r") == False:
                return True

        return False

class JBLexerState(object):
    def __init__(self, code, transitions=None, isStartState=False, isFinalState=False, inputIndex=0):
        super(JBLexerState, self).__init__()
        self.code = code
        self.transitions = transitions
        self.isStartState = isStartState 
        self.isFinalState = isFinalState
        self.inputIndex = inputIndex  

        if self.transitions == None:
            self.transitions = []

    def __repr__(self):
        return "{}({}, {}, {}, {}, {})".format(type(self).__name__, self.code, 
            self.transitions, self.isStartState, self.isFinalState, self.inputIndex)

class JBLexerAction(object):
    ActionToken, ActionIgnoreToken, ActionSaveText, ActionSaveSubText = (0, 1, 2, 3)

    def __init__(self, code, args=None):
        super(JBLexerAction, self).__init__()
        self.code = code
        self.args = args

    def __repr__(self):
        return "{}({}, {})".format(type(self).__name__, self.code, self.args)

# TODO: (maybe) Change tokenCode to tokenInt
class JBLexerRule(object):
    def __init__(self, name, pattern, tokenCode, startState, startConditions=None, setCondition=None, action=None):
        super(JBLexerRule, self).__init__()
        self.name = name
        self.pattern = pattern
        self.tokenCode = tokenCode
        self.startState = startState
        self.startConditions = startConditions
        self.setCondition = setCondition
        self.action = action

        if self.startConditions == None:
            self.startConditions = [0]

    def __repr__(self):
        return "{}({}, {}, {}, {}, {}, {}, {})".format(type(self).__name__, self.name, self.pattern, 
            self.tokenCode, self.startState, self.startConditions, self.setCondition, self.action)

    def match(self, string, startIndex=0):
        self.startState.inputIndex = startIndex

        stack = JBStack()
        stack.push(self.startState)

        stop = 0

        lastValidState = self.startState
        while (stack.isEmpty() == False):
            currState = stack.pop()

            i = currState.inputIndex

            if i >= len(string):
                break

            c = string[i]

            epsilonList = []
            nonEpsilonListChar = []
            nonEpsilonListCharClass = []
            for t in reversed(currState.transitions):
                if t.isMatch(c):
                    s = t.nextState

                    if t.lexerToken.code == JBLexerToken.EPSILON:
                        s.inputIndex = i
                        epsilonList.append(s)
                    else:
                        s.inputIndex = i + 1

                        if t.lexerToken.code == JBLexerToken.CHAR:
                            nonEpsilonListChar.append(s)
                        else:
                            nonEpsilonListCharClass.append(s)

                        lastValidState = s

            for s in nonEpsilonListCharClass:
                stack.push(s)
            for s in nonEpsilonListChar:
                stack.push(s)
            for s in epsilonList:
                stack.push(s)

            if stop > 500:
                print "This should not have happened!!!"
                break

            stop += 1

        # Reach a accepting state only by epsilons
        stack = JBStack()
        stack.push(lastValidState)
        success = False
        out = 0
        while stack.isNotEmpty():
            currState = stack.pop()

            if currState.isFinalState:
                success = True
                break

            for t in reversed(currState.transitions):
                if t.lexerToken.code == JBLexerToken.EPSILON:
                    stack.push(t.nextState)

            if out > 50:
                print "This should not have happened!!!"
                break

            out += 1

        return [success, lastValidState.inputIndex]

class JBLexer(object):
    def __init__(self):
        super(JBLexer, self).__init__()
        self.rules = None
        self.startCondition = 0
        self.currStartCondition = self.startCondition
        self.currIndex = 0
        self.string = None

        self.initRules()

    def __repr__(self):
        return "{}({}, {})".format(type(self).__name__, self.rules, self.startCondition)

    # Private
    def initRules(self):
        rules = []
        
        s_0000 = JBLexerState(0, None, True, False, 0)
        s_0001 = JBLexerState(1, None, False, True, 0)
        s_0002 = JBLexerState(2, None, False, False, 0)
        s_0003 = JBLexerState(3, None, False, False, 0)
        s_0004 = JBLexerState(4, None, False, False, 0)
        s_0005 = JBLexerState(5, None, False, False, 0)
        s_0006 = JBLexerState(6, None, False, False, 0)
        s_0007 = JBLexerState(7, None, False, False, 0)
        s_0008 = JBLexerState(8, None, False, False, 0)
        s_0009 = JBLexerState(9, None, False, False, 0)
        s_0010 = JBLexerState(10, None, False, False, 0)
        s_0011 = JBLexerState(11, None, False, False, 0)
        s_0012 = JBLexerState(12, None, False, False, 0)
        s_0013 = JBLexerState(13, None, False, False, 0)
        s_0014 = JBLexerState(14, None, False, False, 0)
        
        s_0000.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0002)]
        s_0001.transitions = []
        s_0002.transitions = [JBLexerTransition(JBLexerToken(8, 'Nn'), s_0003)]
        s_0003.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0004)]
        s_0004.transitions = [JBLexerTransition(JBLexerToken(8, 'Aa'), s_0005)]
        s_0005.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0006)]
        s_0006.transitions = [JBLexerTransition(JBLexerToken(8, 'Mm'), s_0007)]
        s_0007.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0008)]
        s_0008.transitions = [JBLexerTransition(JBLexerToken(8, 'Ee'), s_0009)]
        s_0009.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0010)]
        s_0010.transitions = [JBLexerTransition(JBLexerToken(0, ':'), s_0011)]
        s_0011.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0012),
        	JBLexerTransition(JBLexerToken(1, None), s_0014)]
        s_0012.transitions = [JBLexerTransition(JBLexerToken(6, None), s_0013)]
        s_0013.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0012),
        	JBLexerTransition(JBLexerToken(1, None), s_0014)]
        s_0014.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0001)]
        
        rules.append(JBLexerRule('OPT_PARSER_NAME', '[Nn][Aa][Mm][Ee]:\s*', 4, s_0000, [0], None, JBLexerAction(0, None)))
        
        s_0015 = JBLexerState(15, None, True, False, 0)
        s_0016 = JBLexerState(16, None, False, True, 0)
        s_0017 = JBLexerState(17, None, False, False, 0)
        s_0018 = JBLexerState(18, None, False, False, 0)
        s_0019 = JBLexerState(19, None, False, False, 0)
        s_0020 = JBLexerState(20, None, False, False, 0)
        s_0021 = JBLexerState(21, None, False, False, 0)
        s_0022 = JBLexerState(22, None, False, False, 0)
        s_0023 = JBLexerState(23, None, False, False, 0)
        s_0024 = JBLexerState(24, None, False, False, 0)
        s_0025 = JBLexerState(25, None, False, False, 0)
        s_0026 = JBLexerState(26, None, False, False, 0)
        s_0028 = JBLexerState(28, None, False, False, 0)
        s_0029 = JBLexerState(29, None, False, False, 0)
        s_0030 = JBLexerState(30, None, False, False, 0)
        s_0031 = JBLexerState(31, None, False, False, 0)
        s_0032 = JBLexerState(32, None, False, False, 0)
        s_0033 = JBLexerState(33, None, False, False, 0)
        s_0034 = JBLexerState(34, None, False, False, 0)
        s_0035 = JBLexerState(35, None, False, False, 0)
        s_0036 = JBLexerState(36, None, False, False, 0)
        s_0037 = JBLexerState(37, None, False, False, 0)
        s_0038 = JBLexerState(38, None, False, False, 0)
        
        s_0015.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0017)]
        s_0016.transitions = []
        s_0017.transitions = [JBLexerTransition(JBLexerToken(8, 'Ll'), s_0018)]
        s_0018.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0019)]
        s_0019.transitions = [JBLexerTransition(JBLexerToken(8, 'Aa'), s_0020)]
        s_0020.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0021)]
        s_0021.transitions = [JBLexerTransition(JBLexerToken(8, 'Nn'), s_0022)]
        s_0022.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0023)]
        s_0023.transitions = [JBLexerTransition(JBLexerToken(8, 'Gg'), s_0024)]
        s_0024.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0025),
        	JBLexerTransition(JBLexerToken(1, None), s_0036)]
        s_0025.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0028)]
        s_0026.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0036)]
        s_0028.transitions = [JBLexerTransition(JBLexerToken(8, 'Uu'), s_0029)]
        s_0029.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0030)]
        s_0030.transitions = [JBLexerTransition(JBLexerToken(8, 'Aa'), s_0031)]
        s_0031.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0032)]
        s_0032.transitions = [JBLexerTransition(JBLexerToken(8, 'Gg'), s_0033)]
        s_0033.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0034)]
        s_0034.transitions = [JBLexerTransition(JBLexerToken(8, 'Ee'), s_0035)]
        s_0035.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0026)]
        s_0036.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0037)]
        s_0037.transitions = [JBLexerTransition(JBLexerToken(0, ':'), s_0038)]
        s_0038.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0016)]
        
        rules.append(JBLexerRule('OPT_OUTPUT_LANGUAGE', '[Ll][Aa][Nn][Gg]([Uu][Aa][Gg][Ee])?:', 4, s_0015, [0], None, JBLexerAction(0, None)))
        
        s_0039 = JBLexerState(39, None, True, False, 0)
        s_0040 = JBLexerState(40, None, False, True, 0)
        s_0041 = JBLexerState(41, None, False, False, 0)
        s_0042 = JBLexerState(42, None, False, False, 0)
        s_0043 = JBLexerState(43, None, False, False, 0)
        s_0044 = JBLexerState(44, None, False, False, 0)
        s_0045 = JBLexerState(45, None, False, False, 0)
        s_0046 = JBLexerState(46, None, False, False, 0)
        s_0047 = JBLexerState(47, None, False, False, 0)
        s_0048 = JBLexerState(48, None, False, False, 0)
        s_0049 = JBLexerState(49, None, False, False, 0)
        s_0050 = JBLexerState(50, None, False, False, 0)
        s_0051 = JBLexerState(51, None, False, False, 0)
        s_0052 = JBLexerState(52, None, False, False, 0)
        s_0053 = JBLexerState(53, None, False, False, 0)
        s_0054 = JBLexerState(54, None, False, False, 0)
        s_0055 = JBLexerState(55, None, False, False, 0)
        s_0056 = JBLexerState(56, None, False, False, 0)
        s_0057 = JBLexerState(57, None, False, False, 0)
        s_0058 = JBLexerState(58, None, False, False, 0)
        s_0059 = JBLexerState(59, None, False, False, 0)
        s_0060 = JBLexerState(60, None, False, False, 0)
        s_0061 = JBLexerState(61, None, False, False, 0)
        s_0062 = JBLexerState(62, None, False, False, 0)
        s_0063 = JBLexerState(63, None, False, False, 0)
        s_0064 = JBLexerState(64, None, False, False, 0)
        s_0065 = JBLexerState(65, None, False, False, 0)
        s_0066 = JBLexerState(66, None, False, False, 0)
        s_0067 = JBLexerState(67, None, False, False, 0)
        s_0068 = JBLexerState(68, None, False, False, 0)
        s_0069 = JBLexerState(69, None, False, False, 0)
        s_0070 = JBLexerState(70, None, False, False, 0)
        
        s_0039.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0041),
        	JBLexerTransition(JBLexerToken(1, None), s_0043)]
        s_0040.transitions = []
        s_0041.transitions = [JBLexerTransition(JBLexerToken(6, None), s_0042)]
        s_0042.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0041),
        	JBLexerTransition(JBLexerToken(1, None), s_0043)]
        s_0043.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0044)]
        s_0044.transitions = [JBLexerTransition(JBLexerToken(8, 'Dd'), s_0045)]
        s_0045.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0046)]
        s_0046.transitions = [JBLexerTransition(JBLexerToken(8, 'Ee'), s_0047)]
        s_0047.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0048)]
        s_0048.transitions = [JBLexerTransition(JBLexerToken(8, 'Ff'), s_0049)]
        s_0049.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0050)]
        s_0050.transitions = [JBLexerTransition(JBLexerToken(8, 'Ii'), s_0051)]
        s_0051.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0052)]
        s_0052.transitions = [JBLexerTransition(JBLexerToken(8, 'Nn'), s_0053)]
        s_0053.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0054)]
        s_0054.transitions = [JBLexerTransition(JBLexerToken(8, 'Ii'), s_0055)]
        s_0055.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0056)]
        s_0056.transitions = [JBLexerTransition(JBLexerToken(8, 'Tt'), s_0057)]
        s_0057.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0058)]
        s_0058.transitions = [JBLexerTransition(JBLexerToken(8, 'Ii'), s_0059)]
        s_0059.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0060)]
        s_0060.transitions = [JBLexerTransition(JBLexerToken(8, 'Oo'), s_0061)]
        s_0061.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0062)]
        s_0062.transitions = [JBLexerTransition(JBLexerToken(8, 'Nn'), s_0063)]
        s_0063.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0064)]
        s_0064.transitions = [JBLexerTransition(JBLexerToken(8, 'Ss'), s_0065)]
        s_0065.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0066)]
        s_0066.transitions = [JBLexerTransition(JBLexerToken(0, ':'), s_0067)]
        s_0067.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0068)]
        s_0068.transitions = [JBLexerTransition(JBLexerToken(6, None), s_0069)]
        s_0069.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0068),
        	JBLexerTransition(JBLexerToken(1, None), s_0070)]
        s_0070.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0040)]
        
        rules.append(JBLexerRule('DEFINITIONS_HEADER', '\s*[Dd][Ee][Ff][Ii][Nn][Ii][Tt][Ii][Oo][Nn][Ss]:\s+', 20, s_0039, [0], None, JBLexerAction(0, None)))
        
        s_0071 = JBLexerState(71, None, True, False, 0)
        s_0072 = JBLexerState(72, None, False, True, 0)
        s_0073 = JBLexerState(73, None, False, False, 0)
        s_0074 = JBLexerState(74, None, False, False, 0)
        s_0075 = JBLexerState(75, None, False, False, 0)
        s_0076 = JBLexerState(76, None, False, False, 0)
        s_0077 = JBLexerState(77, None, False, False, 0)
        s_0078 = JBLexerState(78, None, False, False, 0)
        s_0079 = JBLexerState(79, None, False, False, 0)
        s_0080 = JBLexerState(80, None, False, False, 0)
        s_0081 = JBLexerState(81, None, False, False, 0)
        s_0082 = JBLexerState(82, None, False, False, 0)
        s_0083 = JBLexerState(83, None, False, False, 0)
        s_0084 = JBLexerState(84, None, False, False, 0)
        s_0085 = JBLexerState(85, None, False, False, 0)
        s_0086 = JBLexerState(86, None, False, False, 0)
        s_0087 = JBLexerState(87, None, False, False, 0)
        
        s_0071.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0073)]
        s_0072.transitions = []
        s_0073.transitions = [JBLexerTransition(JBLexerToken(8, 'Rr'), s_0074)]
        s_0074.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0075)]
        s_0075.transitions = [JBLexerTransition(JBLexerToken(8, 'Uu'), s_0076)]
        s_0076.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0077)]
        s_0077.transitions = [JBLexerTransition(JBLexerToken(8, 'Ll'), s_0078)]
        s_0078.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0079)]
        s_0079.transitions = [JBLexerTransition(JBLexerToken(8, 'Ee'), s_0080)]
        s_0080.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0081)]
        s_0081.transitions = [JBLexerTransition(JBLexerToken(8, 'Ss'), s_0082)]
        s_0082.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0083)]
        s_0083.transitions = [JBLexerTransition(JBLexerToken(0, ':'), s_0084)]
        s_0084.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0085)]
        s_0085.transitions = [JBLexerTransition(JBLexerToken(6, None), s_0086)]
        s_0086.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0085),
        	JBLexerTransition(JBLexerToken(1, None), s_0087)]
        s_0087.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0072)]
        
        rules.append(JBLexerRule('RULES_HEADER', '[Rr][Uu][Ll][Ee][Ss]:\s+', 24, s_0071, [0], None, JBLexerAction(0, None)))
        
        s_0088 = JBLexerState(88, None, True, False, 0)
        s_0089 = JBLexerState(89, None, False, True, 0)
        s_0090 = JBLexerState(90, None, False, False, 0)
        s_0091 = JBLexerState(91, None, False, False, 0)
        s_0092 = JBLexerState(92, None, False, False, 0)
        s_0093 = JBLexerState(93, None, False, False, 0)
        s_0094 = JBLexerState(94, None, False, False, 0)
        s_0095 = JBLexerState(95, None, False, False, 0)
        s_0096 = JBLexerState(96, None, False, False, 0)
        s_0097 = JBLexerState(97, None, False, False, 0)
        s_0098 = JBLexerState(98, None, False, False, 0)
        s_0099 = JBLexerState(99, None, False, False, 0)
        s_0100 = JBLexerState(100, None, False, False, 0)
        s_0101 = JBLexerState(101, None, False, False, 0)
        s_0102 = JBLexerState(102, None, False, False, 0)
        s_0103 = JBLexerState(103, None, False, False, 0)
        s_0104 = JBLexerState(104, None, False, False, 0)
        s_0105 = JBLexerState(105, None, False, False, 0)
        s_0106 = JBLexerState(106, None, False, False, 0)
        s_0107 = JBLexerState(107, None, False, False, 0)
        s_0108 = JBLexerState(108, None, False, False, 0)
        
        s_0088.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0090)]
        s_0089.transitions = []
        s_0090.transitions = [JBLexerTransition(JBLexerToken(8, 'Gg'), s_0091)]
        s_0091.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0092)]
        s_0092.transitions = [JBLexerTransition(JBLexerToken(8, 'Rr'), s_0093)]
        s_0093.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0094)]
        s_0094.transitions = [JBLexerTransition(JBLexerToken(8, 'Aa'), s_0095)]
        s_0095.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0096)]
        s_0096.transitions = [JBLexerTransition(JBLexerToken(8, 'Mm'), s_0097)]
        s_0097.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0098)]
        s_0098.transitions = [JBLexerTransition(JBLexerToken(8, 'Mm'), s_0099)]
        s_0099.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0100)]
        s_0100.transitions = [JBLexerTransition(JBLexerToken(8, 'Aa'), s_0101)]
        s_0101.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0102)]
        s_0102.transitions = [JBLexerTransition(JBLexerToken(8, 'Rr'), s_0103)]
        s_0103.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0104)]
        s_0104.transitions = [JBLexerTransition(JBLexerToken(0, ':'), s_0105)]
        s_0105.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0106)]
        s_0106.transitions = [JBLexerTransition(JBLexerToken(6, None), s_0107)]
        s_0107.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0106),
        	JBLexerTransition(JBLexerToken(1, None), s_0108)]
        s_0108.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0089)]
        
        rules.append(JBLexerRule('GRAMMAR_HEADER', '[Gg][Rr][Aa][Mm][Mm][Aa][Rr]:\s+', 5, s_0088, [0], None, JBLexerAction(0, None)))
        
        s_0109 = JBLexerState(109, None, True, False, 0)
        s_0110 = JBLexerState(110, None, False, True, 0)
        s_0111 = JBLexerState(111, None, False, False, 0)
        s_0112 = JBLexerState(112, None, False, False, 0)
        s_0113 = JBLexerState(113, None, False, False, 0)
        s_0114 = JBLexerState(114, None, False, False, 0)
        
        s_0109.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0111)]
        s_0110.transitions = []
        s_0111.transitions = [JBLexerTransition(JBLexerToken(0, '%'), s_0112)]
        s_0112.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0113)]
        s_0113.transitions = [JBLexerTransition(JBLexerToken(0, '{'), s_0114)]
        s_0114.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0110)]
        
        rules.append(JBLexerRule('PROD_ACTION_BEGIN', '%\{', 15, s_0109, [0], None, JBLexerAction(0, None)))
        
        s_0115 = JBLexerState(115, None, True, False, 0)
        s_0116 = JBLexerState(116, None, False, True, 0)
        s_0117 = JBLexerState(117, None, False, False, 0)
        s_0118 = JBLexerState(118, None, False, False, 0)
        s_0119 = JBLexerState(119, None, False, False, 0)
        s_0120 = JBLexerState(120, None, False, False, 0)
        
        s_0115.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0117)]
        s_0116.transitions = []
        s_0117.transitions = [JBLexerTransition(JBLexerToken(0, '%'), s_0118)]
        s_0118.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0119)]
        s_0119.transitions = [JBLexerTransition(JBLexerToken(0, '}'), s_0120)]
        s_0120.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0116)]
        
        rules.append(JBLexerRule('PROD_ACTION_END', '%\}', 18, s_0115, [0], None, JBLexerAction(0, None)))
        
        s_0121 = JBLexerState(121, None, True, False, 0)
        s_0122 = JBLexerState(122, None, False, True, 0)
        s_0123 = JBLexerState(123, None, False, False, 0)
        s_0124 = JBLexerState(124, None, False, False, 0)
        s_0125 = JBLexerState(125, None, False, False, 0)
        s_0126 = JBLexerState(126, None, False, False, 0)
        s_0127 = JBLexerState(127, None, False, False, 0)
        
        s_0121.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0123)]
        s_0122.transitions = []
        s_0123.transitions = [JBLexerTransition(JBLexerToken(0, '|'), s_0124)]
        s_0124.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0125),
        	JBLexerTransition(JBLexerToken(1, None), s_0127)]
        s_0125.transitions = [JBLexerTransition(JBLexerToken(6, None), s_0126)]
        s_0126.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0125),
        	JBLexerTransition(JBLexerToken(1, None), s_0127)]
        s_0127.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0122)]
        
        rules.append(JBLexerRule('PIPE', '\|\s*', 12, s_0121, [0], None, JBLexerAction(0, None)))
        
        s_0128 = JBLexerState(128, None, True, False, 0)
        s_0129 = JBLexerState(129, None, False, True, 0)
        s_0130 = JBLexerState(130, None, False, False, 0)
        s_0131 = JBLexerState(131, None, False, False, 0)
        
        s_0128.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0130)]
        s_0129.transitions = []
        s_0130.transitions = [JBLexerTransition(JBLexerToken(0, '['), s_0131)]
        s_0131.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0129)]
        
        rules.append(JBLexerRule('L_BRACKET', '\[', 16, s_0128, [0], None, JBLexerAction(0, None)))
        
        s_0132 = JBLexerState(132, None, True, False, 0)
        s_0133 = JBLexerState(133, None, False, True, 0)
        s_0134 = JBLexerState(134, None, False, False, 0)
        s_0135 = JBLexerState(135, None, False, False, 0)
        
        s_0132.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0134)]
        s_0133.transitions = []
        s_0134.transitions = [JBLexerTransition(JBLexerToken(0, ']'), s_0135)]
        s_0135.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0133)]
        
        rules.append(JBLexerRule('R_BRACKET', '\]', 3, s_0132, [0], None, JBLexerAction(0, None)))
        
        s_0136 = JBLexerState(136, None, True, False, 0)
        s_0137 = JBLexerState(137, None, False, True, 0)
        s_0138 = JBLexerState(138, None, False, False, 0)
        s_0139 = JBLexerState(139, None, False, False, 0)
        
        s_0136.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0138)]
        s_0137.transitions = []
        s_0138.transitions = [JBLexerTransition(JBLexerToken(0, '('), s_0139)]
        s_0139.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0137)]
        
        rules.append(JBLexerRule('LP', '\(', 14, s_0136, [0], None, JBLexerAction(0, None)))
        
        s_0140 = JBLexerState(140, None, True, False, 0)
        s_0141 = JBLexerState(141, None, False, True, 0)
        s_0142 = JBLexerState(142, None, False, False, 0)
        s_0143 = JBLexerState(143, None, False, False, 0)
        
        s_0140.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0142)]
        s_0141.transitions = []
        s_0142.transitions = [JBLexerTransition(JBLexerToken(0, ')'), s_0143)]
        s_0143.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0141)]
        
        rules.append(JBLexerRule('RP', '\)', 10, s_0140, [0], None, JBLexerAction(0, None)))
        
        s_0144 = JBLexerState(144, None, True, False, 0)
        s_0145 = JBLexerState(145, None, False, True, 0)
        s_0146 = JBLexerState(146, None, False, False, 0)
        s_0147 = JBLexerState(147, None, False, False, 0)
        
        s_0144.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0146)]
        s_0145.transitions = []
        s_0146.transitions = [JBLexerTransition(JBLexerToken(0, '<'), s_0147)]
        s_0147.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0145)]
        
        rules.append(JBLexerRule('LESS', '<', 1, s_0144, [0], None, JBLexerAction(0, None)))
        
        s_0148 = JBLexerState(148, None, True, False, 0)
        s_0149 = JBLexerState(149, None, False, True, 0)
        s_0150 = JBLexerState(150, None, False, False, 0)
        s_0151 = JBLexerState(151, None, False, False, 0)
        
        s_0148.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0150)]
        s_0149.transitions = []
        s_0150.transitions = [JBLexerTransition(JBLexerToken(0, '>'), s_0151)]
        s_0151.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0149)]
        
        rules.append(JBLexerRule('GREATER', '>', 22, s_0148, [0], None, JBLexerAction(0, None)))
        
        s_0152 = JBLexerState(152, None, True, False, 0)
        s_0153 = JBLexerState(153, None, False, True, 0)
        s_0154 = JBLexerState(154, None, False, False, 0)
        s_0155 = JBLexerState(155, None, False, False, 0)
        
        s_0152.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0154)]
        s_0153.transitions = []
        s_0154.transitions = [JBLexerTransition(JBLexerToken(0, ','), s_0155)]
        s_0155.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0153)]
        
        rules.append(JBLexerRule('COMMA', ',', 25, s_0152, [0], None, JBLexerAction(0, None)))
        
        s_0156 = JBLexerState(156, None, True, False, 0)
        s_0157 = JBLexerState(157, None, False, True, 0)
        s_0158 = JBLexerState(158, None, False, False, 0)
        s_0159 = JBLexerState(159, None, False, False, 0)
        
        s_0156.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0158)]
        s_0157.transitions = []
        s_0158.transitions = [JBLexerTransition(JBLexerToken(0, ':'), s_0159)]
        s_0159.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0157)]
        
        rules.append(JBLexerRule('COLON', ':', 13, s_0156, [0], None, JBLexerAction(0, None)))
        
        s_0160 = JBLexerState(160, None, True, False, 0)
        s_0161 = JBLexerState(161, None, False, True, 0)
        s_0162 = JBLexerState(162, None, False, False, 0)
        s_0163 = JBLexerState(163, None, False, False, 0)
        s_0164 = JBLexerState(164, None, False, False, 0)
        s_0165 = JBLexerState(165, None, False, False, 0)
        s_0166 = JBLexerState(166, None, False, False, 0)
        s_0167 = JBLexerState(167, None, False, False, 0)
        s_0168 = JBLexerState(168, None, False, False, 0)
        s_0169 = JBLexerState(169, None, False, False, 0)
        s_0170 = JBLexerState(170, None, False, False, 0)
        s_0171 = JBLexerState(171, None, False, False, 0)
        s_0172 = JBLexerState(172, None, False, False, 0)
        s_0173 = JBLexerState(173, None, False, False, 0)
        s_0174 = JBLexerState(174, None, False, False, 0)
        s_0175 = JBLexerState(175, None, False, False, 0)
        
        s_0160.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0162)]
        s_0161.transitions = []
        s_0162.transitions = [JBLexerTransition(JBLexerToken(0, 'E'), s_0163)]
        s_0163.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0164)]
        s_0164.transitions = [JBLexerTransition(JBLexerToken(0, 'P'), s_0165)]
        s_0165.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0166)]
        s_0166.transitions = [JBLexerTransition(JBLexerToken(0, 'S'), s_0167)]
        s_0167.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0168)]
        s_0168.transitions = [JBLexerTransition(JBLexerToken(0, 'I'), s_0169)]
        s_0169.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0170)]
        s_0170.transitions = [JBLexerTransition(JBLexerToken(0, 'L'), s_0171)]
        s_0171.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0172)]
        s_0172.transitions = [JBLexerTransition(JBLexerToken(0, 'O'), s_0173)]
        s_0173.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0174)]
        s_0174.transitions = [JBLexerTransition(JBLexerToken(0, 'N'), s_0175)]
        s_0175.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0161)]
        
        rules.append(JBLexerRule('EPSILON', 'EPSILON', 23, s_0160, [0], None, JBLexerAction(0, None)))
        
        s_0176 = JBLexerState(176, None, True, False, 0)
        s_0177 = JBLexerState(177, None, False, True, 0)
        s_0178 = JBLexerState(178, None, False, False, 0)
        s_0179 = JBLexerState(179, None, False, False, 0)
        s_0180 = JBLexerState(180, None, False, False, 0)
        s_0181 = JBLexerState(181, None, False, False, 0)
        s_0182 = JBLexerState(182, None, False, False, 0)
        s_0183 = JBLexerState(183, None, False, False, 0)
        s_0184 = JBLexerState(184, None, False, False, 0)
        s_0185 = JBLexerState(185, None, False, False, 0)
        s_0186 = JBLexerState(186, None, False, False, 0)
        s_0187 = JBLexerState(187, None, False, False, 0)
        
        s_0176.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0178)]
        s_0177.transitions = []
        s_0178.transitions = [JBLexerTransition(JBLexerToken(0, 'B'), s_0179)]
        s_0179.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0180)]
        s_0180.transitions = [JBLexerTransition(JBLexerToken(0, 'E'), s_0181)]
        s_0181.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0182)]
        s_0182.transitions = [JBLexerTransition(JBLexerToken(0, 'G'), s_0183)]
        s_0183.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0184)]
        s_0184.transitions = [JBLexerTransition(JBLexerToken(0, 'I'), s_0185)]
        s_0185.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0186)]
        s_0186.transitions = [JBLexerTransition(JBLexerToken(0, 'N'), s_0187)]
        s_0187.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0177)]
        
        rules.append(JBLexerRule('BEGIN', 'BEGIN', 9, s_0176, [0], None, JBLexerAction(0, None)))
        
        s_0188 = JBLexerState(188, None, True, False, 0)
        s_0189 = JBLexerState(189, None, False, True, 0)
        s_0190 = JBLexerState(190, None, False, False, 0)
        s_0191 = JBLexerState(191, None, False, False, 0)
        s_0192 = JBLexerState(192, None, False, False, 0)
        s_0193 = JBLexerState(193, None, False, False, 0)
        s_0194 = JBLexerState(194, None, False, False, 0)
        s_0195 = JBLexerState(195, None, False, False, 0)
        s_0196 = JBLexerState(196, None, False, False, 0)
        s_0197 = JBLexerState(197, None, False, False, 0)
        s_0198 = JBLexerState(198, None, False, False, 0)
        s_0199 = JBLexerState(199, None, False, False, 0)
        
        s_0188.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0190)]
        s_0189.transitions = []
        s_0190.transitions = [JBLexerTransition(JBLexerToken(0, 'T'), s_0191)]
        s_0191.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0192)]
        s_0192.transitions = [JBLexerTransition(JBLexerToken(0, 'O'), s_0193)]
        s_0193.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0194)]
        s_0194.transitions = [JBLexerTransition(JBLexerToken(0, 'K'), s_0195)]
        s_0195.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0196)]
        s_0196.transitions = [JBLexerTransition(JBLexerToken(0, 'E'), s_0197)]
        s_0197.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0198)]
        s_0198.transitions = [JBLexerTransition(JBLexerToken(0, 'N'), s_0199)]
        s_0199.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0189)]
        
        rules.append(JBLexerRule('TOKEN', 'TOKEN', 19, s_0188, [0], None, JBLexerAction(0, None)))
        
        s_0200 = JBLexerState(200, None, True, False, 0)
        s_0201 = JBLexerState(201, None, False, True, 0)
        s_0202 = JBLexerState(202, None, False, False, 0)
        s_0203 = JBLexerState(203, None, False, False, 0)
        s_0204 = JBLexerState(204, None, False, False, 0)
        s_0205 = JBLexerState(205, None, False, False, 0)
        s_0206 = JBLexerState(206, None, False, False, 0)
        s_0207 = JBLexerState(207, None, False, False, 0)
        s_0208 = JBLexerState(208, None, False, False, 0)
        s_0209 = JBLexerState(209, None, False, False, 0)
        s_0210 = JBLexerState(210, None, False, False, 0)
        s_0211 = JBLexerState(211, None, False, False, 0)
        s_0212 = JBLexerState(212, None, False, False, 0)
        s_0213 = JBLexerState(213, None, False, False, 0)
        s_0214 = JBLexerState(214, None, False, False, 0)
        s_0215 = JBLexerState(215, None, False, False, 0)
        s_0216 = JBLexerState(216, None, False, False, 0)
        s_0217 = JBLexerState(217, None, False, False, 0)
        s_0218 = JBLexerState(218, None, False, False, 0)
        s_0219 = JBLexerState(219, None, False, False, 0)
        s_0220 = JBLexerState(220, None, False, False, 0)
        s_0221 = JBLexerState(221, None, False, False, 0)
        
        s_0200.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0202),
        	JBLexerTransition(JBLexerToken(1, None), s_0214)]
        s_0201.transitions = []
        s_0202.transitions = [JBLexerTransition(JBLexerToken(0, 'I'), s_0203)]
        s_0203.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0204)]
        s_0204.transitions = [JBLexerTransition(JBLexerToken(0, 'G'), s_0205)]
        s_0205.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0206)]
        s_0206.transitions = [JBLexerTransition(JBLexerToken(0, 'N'), s_0207)]
        s_0207.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0208)]
        s_0208.transitions = [JBLexerTransition(JBLexerToken(0, 'O'), s_0209)]
        s_0209.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0210)]
        s_0210.transitions = [JBLexerTransition(JBLexerToken(0, 'R'), s_0211)]
        s_0211.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0212)]
        s_0212.transitions = [JBLexerTransition(JBLexerToken(0, 'E'), s_0213)]
        s_0213.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0201)]
        s_0214.transitions = [JBLexerTransition(JBLexerToken(0, 'S'), s_0215)]
        s_0215.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0216)]
        s_0216.transitions = [JBLexerTransition(JBLexerToken(0, 'A'), s_0217)]
        s_0217.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0218)]
        s_0218.transitions = [JBLexerTransition(JBLexerToken(0, 'V'), s_0219)]
        s_0219.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0220)]
        s_0220.transitions = [JBLexerTransition(JBLexerToken(0, 'E'), s_0221)]
        s_0221.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0201)]
        
        rules.append(JBLexerRule('TOKEN_ACTION', 'IGNORE|SAVE', 8, s_0200, [0], None, JBLexerAction(2, None)))
        
        s_0222 = JBLexerState(222, None, True, False, 0)
        s_0223 = JBLexerState(223, None, False, True, 0)
        s_0224 = JBLexerState(224, None, False, False, 0)
        s_0225 = JBLexerState(225, None, False, False, 0)
        s_0226 = JBLexerState(226, None, False, False, 0)
        
        s_0222.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0224)]
        s_0223.transitions = []
        s_0224.transitions = [JBLexerTransition(JBLexerToken(2, None), s_0225)]
        s_0225.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0224),
        	JBLexerTransition(JBLexerToken(1, None), s_0226)]
        s_0226.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0223)]
        
        rules.append(JBLexerRule('STRING', '\w+', 7, s_0222, [0], None, JBLexerAction(2, None)))
        
        s_0227 = JBLexerState(227, None, True, False, 0)
        s_0228 = JBLexerState(228, None, False, True, 0)
        s_0229 = JBLexerState(229, None, False, False, 0)
        s_0230 = JBLexerState(230, None, False, False, 0)
        s_0231 = JBLexerState(231, None, False, False, 0)
        s_0232 = JBLexerState(232, None, False, False, 0)
        s_0233 = JBLexerState(233, None, False, False, 0)
        s_0234 = JBLexerState(234, None, False, False, 0)
        s_0235 = JBLexerState(235, None, False, False, 0)
        
        s_0227.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0229)]
        s_0228.transitions = []
        s_0229.transitions = [JBLexerTransition(JBLexerToken(0, '"'), s_0230)]
        s_0230.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0231)]
        s_0231.transitions = [JBLexerTransition(JBLexerToken(9, '"'), s_0232)]
        s_0232.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0231),
        	JBLexerTransition(JBLexerToken(1, None), s_0233)]
        s_0233.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0234)]
        s_0234.transitions = [JBLexerTransition(JBLexerToken(0, '"'), s_0235)]
        s_0235.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0228)]
        
        rules.append(JBLexerRule('PATTERN', '"[^"]+"', 2, s_0227, [0], None, JBLexerAction(3, [1, -2])))
        
        s_0236 = JBLexerState(236, None, True, False, 0)
        s_0237 = JBLexerState(237, None, False, True, 0)
        s_0238 = JBLexerState(238, None, False, False, 0)
        s_0239 = JBLexerState(239, None, False, False, 0)
        s_0240 = JBLexerState(240, None, False, False, 0)
        s_0241 = JBLexerState(241, None, False, False, 0)
        s_0242 = JBLexerState(242, None, False, False, 0)
        s_0243 = JBLexerState(243, None, False, False, 0)
        
        s_0236.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0238)]
        s_0237.transitions = []
        s_0238.transitions = [JBLexerTransition(JBLexerToken(8, '\r\n'), s_0239)]
        s_0239.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0238),
        	JBLexerTransition(JBLexerToken(1, None), s_0240)]
        s_0240.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0241),
        	JBLexerTransition(JBLexerToken(1, None), s_0243)]
        s_0241.transitions = [JBLexerTransition(JBLexerToken(6, None), s_0242)]
        s_0242.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0241),
        	JBLexerTransition(JBLexerToken(1, None), s_0243)]
        s_0243.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0237)]
        
        rules.append(JBLexerRule('END_STATEMENT', '[\r\n]+\s*', 17, s_0236, [0], None, JBLexerAction(0, None)))
        
        s_0244 = JBLexerState(244, None, True, False, 0)
        s_0245 = JBLexerState(245, None, False, True, 0)
        s_0246 = JBLexerState(246, None, False, False, 0)
        s_0247 = JBLexerState(247, None, False, False, 0)
        s_0248 = JBLexerState(248, None, False, False, 0)
        s_0249 = JBLexerState(249, None, False, False, 0)
        s_0250 = JBLexerState(250, None, False, False, 0)
        
        s_0244.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0246)]
        s_0245.transitions = []
        s_0246.transitions = [JBLexerTransition(JBLexerToken(0, ';'), s_0247)]
        s_0247.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0248),
        	JBLexerTransition(JBLexerToken(1, None), s_0250)]
        s_0248.transitions = [JBLexerTransition(JBLexerToken(6, None), s_0249)]
        s_0249.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0248),
        	JBLexerTransition(JBLexerToken(1, None), s_0250)]
        s_0250.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0245)]
        
        rules.append(JBLexerRule('SEMI_COLON', ';\s*', 6, s_0244, [0], None, JBLexerAction(0, None)))
        
        s_0251 = JBLexerState(251, None, True, False, 0)
        s_0252 = JBLexerState(252, None, False, True, 0)
        s_0253 = JBLexerState(253, None, False, False, 0)
        s_0254 = JBLexerState(254, None, False, False, 0)
        s_0255 = JBLexerState(255, None, False, False, 0)
        
        s_0251.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0253)]
        s_0252.transitions = []
        s_0253.transitions = [JBLexerTransition(JBLexerToken(6, None), s_0254)]
        s_0254.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0253),
        	JBLexerTransition(JBLexerToken(1, None), s_0255)]
        s_0255.transitions = [JBLexerTransition(JBLexerToken(1, None), s_0252)]
        
        rules.append(JBLexerRule('WHITESPACE', '\s+', -1, s_0251, [0], None, JBLexerAction(1, None)))
        
        self.rules = rules

    # Public
    def startLexer(self, string):
        self.string = string
        self.currIndex = 0

    # Public
    def nextToken(self):
        # if not(self.string != None and self.currIndex < len(self.string)):
        #     raise Exception("Tokenizer is not initiated!!!!!!")

        if (self.currIndex < 0) or (len(self.string) == 0):
            raise Exception("Tokenizer is not initiated.")

        if (self.currIndex >= len(self.string)):
            return JBParserToken(JBParserToken.JB_EOS, None)
        
        token = None

        # Only check for rules that has the current start condition
        validRules = [rule for rule in self.rules if (self.currStartCondition in rule.startConditions)]

        for r in validRules:
            success, nextIndex = r.match(self.string, self.currIndex)

            if success:
                if r.action.code == JBLexerAction.ActionIgnoreToken:
                    self.currIndex = nextIndex

                    if r.setCondition != None:
                        self.currStartCondition = r.setCondition

                    token = self.nextToken()
                else:
                    if r.action.code == JBLexerAction.ActionSaveText:
                        token = JBParserToken(r.tokenCode, self.string[self.currIndex:nextIndex])
                    elif r.action.code == JBLexerAction.ActionSaveSubText:
                        maxStringIndex = nextIndex - self.currIndex
    
                        # Validade sub string indexes
                        if (r.action.args != None) and (len(r.action.args) == 2):
                            fromIndex = r.action.args[0]
                            toIndex = r.action.args[1]
                            if fromIndex < 0:
                                fromIndex += maxStringIndex + 1
                            if toIndex < 0:
                                toIndex += maxStringIndex + 1
    
                            if (fromIndex < toIndex) and (toIndex <= maxStringIndex):
                                token = JBParserToken(r.tokenCode, self.string[self.currIndex+fromIndex:self.currIndex+toIndex])
                        else:
                            raise Exception("Invalid indexes for sub string.")
                    else:
                        token = JBParserToken(r.tokenCode)
    
                    self.currIndex = nextIndex
    
                    if r.setCondition != None:
                        self.currStartCondition = r.setCondition
    
                break

        if token == None:
            raise Exception("Parsing error at index = {}, char = '{}'.".format(self.currIndex, self.string[self.currIndex]))

        return token

class JBParser(object):
    def __init__(self):
        super(JBParser, self).__init__()
        self.lexer = JBLexer()
        self.pTable = self.genParsingTable()

    def genParsingTable(self):
        table = {}

        table[JBParserToken.parser] = {JBParserToken.DEFINITIONS_HEADER: 1, JBParserToken.L_BRACKET: 1}
        table[JBParserToken.g_production] = {JBParserToken.STRING: 22}
        table[JBParserToken.optional_arg] = {JBParserToken.OPT_PARSER_NAME: 5, JBParserToken.OPT_OUTPUT_LANGUAGE: 6}
        table[JBParserToken.grammar_section] = {JBParserToken.GRAMMAR_HEADER: 20}
        table[JBParserToken.r_action_more] = {JBParserToken.END_STATEMENT: -1, JBParserToken.COMMA: 16}
        table[JBParserToken.optional_section] = {JBParserToken.DEFINITIONS_HEADER: -1, JBParserToken.L_BRACKET: 2}
        table[JBParserToken.definitions_section] = {JBParserToken.DEFINITIONS_HEADER: 7}
        table[JBParserToken.d_stmt] = {JBParserToken.STRING: 9}
        table[JBParserToken.g_strings_more] = {JBParserToken.PIPE: -1, JBParserToken.SEMI_COLON: -1, JBParserToken.STRING: 26}
        table[JBParserToken.r_statements] = {JBParserToken.PATTERN: 11, JBParserToken.GRAMMAR_HEADER: -1, JBParserToken.LESS: 11}
        table[JBParserToken.optional_args_more] = {JBParserToken.COMMA: 4, JBParserToken.R_BRACKET: -1}
        table[JBParserToken.g_strings] = {JBParserToken.STRING: 25}
        table[JBParserToken.g_right] = {JBParserToken.STRING: 23}
        table[JBParserToken.rules_section] = {JBParserToken.RULES_HEADER: 10}
        table[JBParserToken.g_productions] = {JBParserToken.STRING: 21}
        table[JBParserToken.r_start_conditions] = {JBParserToken.PATTERN: -1, JBParserToken.LESS: 13}
        table[JBParserToken.g_right_more] = {JBParserToken.PIPE: 24, JBParserToken.SEMI_COLON: -1}
        table[JBParserToken.r_actions] = {JBParserToken.BEGIN: 15, JBParserToken.TOKEN: 15}
        table[JBParserToken.d_statements] = {JBParserToken.RULES_HEADER: -1, JBParserToken.STRING: 8}
        table[JBParserToken.r_stmt] = {JBParserToken.PATTERN: 12, JBParserToken.LESS: 12}
        table[JBParserToken.r_start_conditions_more] = {JBParserToken.COMMA: 14, JBParserToken.GREATER: -1}
        table[JBParserToken.optional_args] = {JBParserToken.OPT_PARSER_NAME: 3, JBParserToken.OPT_OUTPUT_LANGUAGE: 3, JBParserToken.R_BRACKET: -1}
        table[JBParserToken.r_action] = {JBParserToken.BEGIN: 17, JBParserToken.TOKEN: 18}
        table[JBParserToken.r_token_more] = {JBParserToken.RP: -1, JBParserToken.COMMA: 19}

        return table

