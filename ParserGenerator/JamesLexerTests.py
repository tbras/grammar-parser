import unittest
import sys
import JamesLexer as JL

class TestParsingTress(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_multiple_regexes(self):
        # Test string with indexes
        rule = JL.JBLexerRule("PATTERN", "ab*|[Hh][Ee][Ll][Ll][Oo]|z.*", 1)

        self.assertEquals(rule.match("babbb", 1), [True, 5])
        self.assertEquals(rule.match("hello", 0), [True, 5])
        self.assertEquals(rule.match("Hello", 0), [True, 5])
        self.assertEquals(rule.match("HELLO", 0), [True, 5])
        self.assertEquals(rule.match("hELLo", 0), [True, 5])
        self.assertEquals(rule.match("z49dkFc_ 1\n590", 0), [True, 10])

        rule = JL.JBLexerRule("PATTERN", "[ \t\v\f]*[\r\n]+\s+", 1)

        self.assertEquals(rule.match("  \t \n\r  \n\n\r\t \n   ", 0), [True, 17])

        rule = JL.JBLexerRule("PATTERN", "[\[\]a-c ]+", 1)

        self.assertEquals(rule.match("a][ ]c b", 0), [True, 8])

        rule = JL.JBLexerRule("PATTERN", "[\r\n]+\s+", 1)

        self.assertEquals(rule.match("\n    \t  ", 0), [True, 8])
        self.assertEquals(rule.match(" \t\n", 0), [False, 0])

        rule = JL.JBLexerRule("PATTERN", "...", 1)

        self.assertEquals(rule.match("tac", 0), [True, 3])
        self.assertEquals(rule.match("ta\n", 0), [False, 2])

        rule = JL.JBLexerRule("PATTERN", "ab*|\[\]\*\+\.-", 1)
        self.assertEquals(rule.match("ab"), [True, 2])
        self.assertEquals(rule.match("a"), [True, 1])
        self.assertEquals(rule.match("b"), [False, 0])
        self.assertEquals(rule.match("abbb"), [True, 4])
        self.assertEquals(rule.match("abcb"), [True, 2])
        self.assertEquals(rule.match("cav"), [False, 0])
        self.assertEquals(rule.match("[]*+.-"), [True, 6])

        rule = JL.JBLexerRule("PATTERN", "(a|b)+c?|(x|y)*|z", 1)
        self.assertEquals(rule.match(""), [True, 0])
        self.assertEquals(rule.match("ac"), [True, 2])
        self.assertEquals(rule.match("a"), [True, 1])
        self.assertEquals(rule.match("b"), [True, 1])
        self.assertEquals(rule.match("bc"), [True, 2])
        self.assertEquals(rule.match("aababbac"), [True, 8])
        self.assertEquals(rule.match("aabacbbab"), [True, 5])
        self.assertEquals(rule.match("abbacd"), [True, 5])
        self.assertEquals(rule.match("xyb"), [True, 2])
        self.assertEquals(rule.match("z"), [True, 1])
        self.assertEquals(rule.match("xxyyxy"), [True, 6])

        # Test character classes
        rule = JL.JBLexerRule("PATTERN", "z[^a-c]+|\w*\||1\w+\s+\w", 1)
        self.assertEquals(rule.match("b|"), [True, 2])
        self.assertEquals(rule.match("bbc|"), [True, 4])
        self.assertEquals(rule.match("|"), [True, 1])
        self.assertEquals(rule.match("a-"), [False, 1])
        self.assertEquals(rule.match("1aa\n  \t \n\rA"), [True, 11])
        self.assertEquals(rule.match("zdCA"), [True, 4])

        # Test ranges
        rule = JL.JBLexerRule("PATTERN", "a[cd]|x[^ab]+|\{\}|[y-z]+[0-9]+[a-cD-F]*", 1)
        self.assertEquals(rule.match("ad"), [True, 2])
        self.assertEquals(rule.match("ac"), [True, 2])
        self.assertEquals(rule.match("xepfpde"), [True, 7])
        self.assertEquals(rule.match("xepfpage"), [True, 5])
        self.assertEquals(rule.match("xa"), [False, 1])
        self.assertEquals(rule.match("{}"), [True, 2])
        self.assertEquals(rule.match("y8"), [True, 2])
        self.assertEquals(rule.match("yz0bbDFaA"), [True, 8])
        self.assertEquals(rule.match("yzzya"), [False, 4])


    def test_lexer(self):

        self.maxDiff = None

        lexer = JL.JBLexer()
        lexer.addRule("COMMA", ",", 1)
        lexer.addRule("Q_MARK", "?", 2)
        lexer.addRule("WORD", "\w+", 3)
        lexer.addRule("SPACE", "\s+", 4)

        tokens = [t.name for t in lexer.tokenizeString("Hello, how are you?")]
        expectedTokens = ["WORD", "COMMA", "SPACE", "WORD", "SPACE", "WORD", "SPACE", "WORD", "Q_MARK"]

        self.assertEquals(tokens, expectedTokens)

        # Test start conditions
        lexer = JL.JBLexer()
        lexer.addRule("SPACE", "\s+", 1)
        lexer.addRule("DSHARP", "^^", 2)
        lexer.addRule("SHARP", "^", 3)
        lexer.addRule("NATURAL", "=", 4)
        lexer.addRule("DFLAT", "__", 5)
        lexer.addRule("FLAT", "_", 6)
        lexer.addRule("NOTE", "[abcdefgABCDEFGrR]", 7)
        lexer.addRule("DIGIT", "\d+", 8)
        lexer.addRule("LCURLY", "\{", 9, setCondition=1)
        lexer.addRule("WORD", "\w+", 10, startConditions=[1])
        lexer.addRule("RCURLY", "\}", 11, startConditions=[1], setCondition=0)
        lexer.addRule("NAME", "\w+", 12)

        tokens = [t.name for t in lexer.tokenizeString("Hello _D{hello}^^e2=a4")]
        expectedTokens = ["NAME", "SPACE", "FLAT", "NOTE", "LCURLY", "WORD", "RCURLY", "DSHARP", "NOTE", "DIGIT", "NATURAL", "NOTE", "DIGIT"]

        self.assertEquals(tokens, expectedTokens)

        actionToken = JL.JBLexerAction(JL.JBLexerAction.ActionToken)
        actionSaveText = JL.JBLexerAction(JL.JBLexerAction.ActionSaveText)
        actionIgnoreToken = JL.JBLexerAction(JL.JBLexerAction.ActionIgnoreToken)

        # Test start conditions, ignored tokens and ActionSaveSubText action
        lexer = JL.JBLexer()
        lexer.addRule("STUPID", "stupid", 1, action=actionToken)
        lexer.addRule("WORD", "\w+", 2, action=actionSaveText)
        lexer.addRule("SPACE", "\s+", 3, action=actionIgnoreToken)
        lexer.addRule("SUB_STR", "<\w+>", 4, action=JL.JBLexerAction(JL.JBLexerAction.ActionSaveSubText, [1, -2]))
        lexer.addRule("SUB_STR_1", "::\w+::", 5, action=JL.JBLexerAction(JL.JBLexerAction.ActionSaveSubText, [2, -3]))

        tokens = lexer.tokenizeString("stupid \t hello world stupid \t <jay> ::papi:: \n")
        nameTokens = [t.name for t in tokens]
        varTokens = [t.var for t in tokens]

        nameExpectedTokens = ["STUPID", "WORD", "WORD", "STUPID", "SUB_STR", "SUB_STR_1"]
        varExpectedTokens = [None, "hello", "world", None, "jay", "papi"]

        self.assertEquals(nameTokens, nameExpectedTokens)
        self.assertEquals(varTokens, varExpectedTokens)

        # Test multiline inputs and ActionSaveSubText action
        lexer = JL.JBLexer()
        lexer.addDefinition("string", "\w+")
        lexer.addRule("D", "[Dd][Ee][Ff][Ii][Nn][Ii][Tt][Ii][Oo][Nn][Ss]:", 2, action=actionToken)
        lexer.addRule("R", "[Rr][Uu][Ll][Ee][Ss]:", 3, action=actionToken)
        lexer.addRule("G", "[Gg][Rr][Aa][Mm][Mm][Aa][Rr]:", 4, action=actionToken)
        lexer.addRule("<", "<", 5, action=actionToken)
        lexer.addRule(">", ">", 6, action=actionToken)
        lexer.addRule(",", ",", 7, action=actionToken)
        lexer.addRule("(", "\(", 8, action=actionToken)
        lexer.addRule(")", "\)", 9, action=actionToken)
        lexer.addRule(":", ":", 10, action=actionToken)
        lexer.addRule("|", "\|", 11, action=actionToken)
        lexer.addRule("BEGIN", "BEGIN", 12, action=actionToken)
        lexer.addRule("TOKEN", "TOKEN", 13, action=actionToken)
        lexer.addRule("PATTERN", "\"[^\"]+\"", 14, action=JL.JBLexerAction(JL.JBLexerAction.ActionSaveSubText, [1, -2]))
        lexer.addRule("STRING", "{string}", 15, action=actionSaveText)
        lexer.addRule("END_STAT", "[\r\n]+\s+", 16, action=actionToken)
        lexer.addRule("IGNORED_TOKEN", "\s+", 17, action=actionIgnoreToken)

        grammarText = """Definitions:
        digit   "[0-9]+"
        string  "\w+"

        Rules:

        "STUFF{string}"             BEGIN(SC1), TOKEN(a1, a2 ,\t s3)
        <SC>"{digit}{string}"       TOKEN(test)
        <SC, SC1  ,\tSC2 >"puff"    TOKEN(test2)

        Grammar:


        hello   :   test | hello

        """

        tokens = lexer.tokenizeString(grammarText)

        nameTokens = [t.name for t in tokens]
        varTokens = [t.var for t in tokens]

        nameExpectedTokens = ["D", "END_STAT", "STRING", "PATTERN", "END_STAT", "STRING", "PATTERN", "END_STAT", 
            "R", "END_STAT", "PATTERN", "BEGIN", "(", "STRING", ")", ",", "TOKEN", "(", "STRING", ",", "STRING", ",", "STRING", ')', "END_STAT",
            "<", "STRING", ">", "PATTERN", "TOKEN", "(", "STRING", ")", "END_STAT",
            "<", "STRING", ",", "STRING", ",", "STRING", ">", "PATTERN", "TOKEN", "(", "STRING", ")", "END_STAT",
            "G", "END_STAT", "STRING", ":", "STRING", "|", "STRING", "END_STAT"]
        varExpectedTokens = [None, None, "digit", "[0-9]+", None, "string", "\w+", None,
            None, None, "STUFF{string}", None, None, "SC1", None, None, None, None, "a1", None, "a2", None, "s3", None, None,
            None, "SC", None, "{digit}{string}", None, None, "test", None, None,
            None, "SC", None, "SC1", None, "SC2", None, "puff", None, None, "test2", None, None,
            None, None, "hello", None, "test", None, "hello", None]

        self.assertEquals(nameTokens, nameExpectedTokens)
        self.assertEquals(varTokens, varExpectedTokens)

        lexer.initTokenizer(grammarText)

        for expectedName, expectedVar in zip(nameExpectedTokens, varExpectedTokens):
            token = lexer.nextToken()

            self.assertEqual(token.name, expectedName)
            self.assertEqual(token.var, expectedVar) 


def runAllTests():
    # Load all tests from module
    alltests = unittest.TestLoader().loadTestsFromModule(sys.modules[__name__])

    unittest.TextTestRunner(verbosity=2).run(alltests)

if __name__ == '__main__':
    runAllTests()