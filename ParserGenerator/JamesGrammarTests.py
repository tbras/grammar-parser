import unittest
import sys
import JamesGrammar as JG
import JamesLexer as JL
import pprint as pp

class TestGrammar(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_symbols(self):
        # Lexer definition
        SC_DEFAULT = 0
        SC_GRAMMAR_SECTION = 1
    
        # Define lexer
        lexer = JL.JBLexer(SC_DEFAULT)
    
        # Lexer definitions
        lexer.addDefinition("word", "[a-zA-Z]+")
        lexer.addDefinition("string", "[a-zA-Z]\w+")
    
        # Sections
        # Igore whitespace in grammar section
        lexer.addRule("WHITESPACE", "\s+", -1, 
            startConditions=[SC_GRAMMAR_SECTION], action=JL.JBLexerAction.ActionIgnoreToken)
    
        lexer.addRule("DEFINITIONS_HEADER", "[Dd][Ee][Ff][Ii][Nn][Ii][Tt][Ii][Oo][Nn][Ss]:\s+", JBToken.TS_DEFINITIONS_HEADER)
        lexer.addRule("RULES_HEADER", "[Rr][Uu][Ll][Ee][Ss]:\s+", JBToken.TS_RULES_HEADER)
        lexer.addRule("GRAMMAR_HEADER", "[Gg][Rr][Aa][Mm][Mm][Aa][Rr]:", JBToken.TS_GRAMMAR_HEADER, setCondition=SC_GRAMMAR_SECTION)
    
        lexer.addRule("PIPE", "\|", JBToken.TS_PIPE)
        lexer.addRule("LP", "\(", JBToken.TS_LP)
        lexer.addRule("RP", "\)", JBToken.TS_RP)
        lexer.addRule("LESS", "<", JBToken.TS_LESS)
        lexer.addRule("GREATER", ">", JBToken.TS_GREATER)
        lexer.addRule("COMMA", ",", JBToken.TS_COMMA)
        lexer.addRule("SEMI_COLON", ";", JBToken.TS_SEMI_COLON)
        lexer.addRule("COLON", ":", JBToken.TS_COLON)
        lexer.addRule("EPSILON", "EPSILON", JBToken.TS_EPSILON)
        lexer.addRule("BEGIN", "BEGIN", JBToken.TS_BEGIN)
        lexer.addRule("TOKEN", "TOKEN", JBToken.TS_TOKEN)
        lexer.addRule("TOKEN_ACTION", "IGNORE|SAVE", JBToken.TS_TOKEN_ACTION, action=JL.JBLexerAction.ActionSaveText)
        lexer.addRule("STRING", "\w+", JBToken.TS_STRING, action=JL.JBLexerAction.ActionSaveText)
        lexer.addRule("PATTERN", "\"[^\"]+\"", JBToken.TS_PATTERN, action=JL.JBLexerAction(JL.JBLexerAction.ActionSaveSubText, [1, -2]))
        lexer.addRule("END_STATEMENT", "[\r\n]+\s+", JBToken.TS_END_STATEMENT)
    
        grammar = JG.JBGrammar(lexer, [
            'parser : definitions_section rules_section grammar_section',
            'definitions_section : DEFINITIONS_HEADER d_statements | EPSILON',
            'd_statements : d_stmt d_statements | EPSILON',
            'd_stmt : STRING PATTERN END_STATEMENT',
            'rules_section : RULES_HEADER r_statements',
            'r_statements : r_stmt r_statements',
            'r_stmt : r_start_conditions STRING r_actions END_STATEMENT',
            'r_start_conditions : LESS STRING r_start_conditions_more GREATER',
            'r_start_conditions_more : COMMA STRING | EPSILON',
            'r_actions : BEGIN LP STRING RP | TOKEN LP STRING r_token_more RP',
            'r_token_more : COMMA TOKEN_ACTION | EPSILON',
            'grammar_section : GRAMMAR_HEADER g_statements',
            'g_statements : g_productions',
            'g_productions : g_production SEMI_COLON',
            'g_production : STRING COLON g_right',
            'g_right : g_strings g_right_more',
            'g_right_more : PIPE g_strings g_right_more | EPSILON',
            'g_strings : STRING g_strings_more',
            'g_strings_more : STRING g_strings_more | EPSILON'
            ])


        expected = {
            'grammar': [
                {'NUM': 2, 'NAME': 'STRING'},
                {'NUM': 3, 'NAME': 'STRING'},
                {'NUM': 1, 'NAME': 'ID'}, 
                {'NUM': 1, 'NAME': 'MINUS'},
                {'NUM': -1, 'NAME': 'EPSILON'}
            ],
            'expr': [
                {'NUM': 4, 'NAME': 'ID'}, 
                {'NUM': 5, 'NAME': 'MINUS'},
                {'NUM': 5, 'NAME': 'ID'},
                {'NUM': -1, 'NAME': 'EPSILON'}
            ], 
            'term': 
            [
                {'NUM': 6, 'NAME': 'MINUS'},
                {'NUM': -1, 'NAME': 'EPSILON'}
            ],
            'job': 
            [
                {'NUM': 7, 'NAME': 'ID'}
            ]
        }

        # print "\nFirst sets:"
        # pp.pprint(grammar.firstSets)
        # print "\nFollow sets:"
        # pp.pprint(grammar.followSets)
        # grammar.validate()
        # print "\nTable:"
        # pp.pprint(grammar.parsingTable)
        # self.assertSets(g.firstSets, expected)
        # pp.pprint(grammar.getListWithLineTokens())

        # self.assertEquals(g.nonTerminals, ['grammar', 'expr'])
        # self.assertEquals(g.terminals, ['+', 'POINT', 'ID', 'NOTHING'])

    def assertSets(self, a, b):
        for key in a:
            if (key in b) == False:
                raise Exception("'{}'' key isn't in {}.".format(key, b))

            for aT in a[key]:
                exists = False
                for bT in b[key]:
                    if aT[JG.JBGrammar.P_NUM] == bT[JG.JBGrammar.P_NUM] and aT[JG.JBGrammar.P_NAME] == bT[JG.JBGrammar.P_NAME]:
                        exists = True
                        break

                if exists == False:
                    raise Exception("'{}': {} doesn't exist in {}.".format(key, aT, b[key]))

            for bT in b[key]:
                exists = False
                for aT in a[key]:
                    if aT[JG.JBGrammar.P_NUM] == bT[JG.JBGrammar.P_NUM] and aT[JG.JBGrammar.P_NAME] == bT[JG.JBGrammar.P_NAME]:
                        exists = True
                        break

                if exists == False:
                    raise Exception("'{}': {} doesn't exist in {}.".format(key, bT, a[key]))

class JBToken(object):
    NTS_grammar_section = 0
    NTS_g_right_more = 1
    NTS_parser = 2
    NTS_g_production = 3
    NTS_d_statements = 4
    NTS_rules_section = 5
    NTS_definitions_section = 6
    NTS_d_stmt = 7
    NTS_r_statements = 8
    NTS_g_strings_more = 9
    NTS_r_start_conditions_more = 10
    NTS_g_strings = 11
    NTS_r_start_conditions = 12
    NTS_r_stmt = 13
    NTS_g_statements = 14
    NTS_g_right = 15
    NTS_g_productions = 16
    NTS_r_token_more = 17
    NTS_r_actions = 18
    TS_EOS = 19
    TS_GRAMMAR_HEADER = 20
    TS_PIPE = 21
    TS_EPSILON = 22
    TS_STRING = 23
    TS_COLON = 24
    TS_RULES_HEADER = 25
    TS_DEFINITIONS_HEADER = 26
    TS_PATTERN = 27
    TS_END_STATEMENT = 28
    TS_COMMA = 29
    TS_LESS = 30
    TS_GREATER = 31
    TS_SEMI_COLON = 32
    TS_TOKEN_ACTION = 33
    TS_BEGIN = 34
    TS_LP = 35
    TS_RP = 36
    TS_TOKEN = 37

    def __init__(self, code, var=None):
        super(JBToken, self).__init__()
        self.code = code
        self.var = var

def runAllTests():
    # Load all tests from module
    alltests = unittest.TestLoader().loadTestsFromModule(sys.modules[__name__])

    unittest.TextTestRunner(verbosity=2).run(alltests)

if __name__ == '__main__':
    runAllTests()