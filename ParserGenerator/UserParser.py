import JamesParser as JP

def main():
    # Example:
    parser = UserParser()

    stringToParse = open('ParsingExample.jbd', 'r').read()

    parser.parseString(stringToParse)

class UserParser(JP.JBParser):
    def __init__(self):
        super(UserParser, self).__init__()

    def parseString(self, string):
        self.lexer.startLexer(string)

        stack = JP.JBStack()  # Symbol Stack

        stack.push(JP.JBParserToken.JB_EOS)
        stack.push(JP.JBParserToken.parser)

        token = self.lexer.nextToken()
        while stack.isNotEmpty():
            if token == None or token.code == JP.JBParserToken.JB_EOS:
                break
            elif token.code == stack.top(): # Match
                # Insert code here
                print str(token)

                token = self.lexer.nextToken()
                stack.pop()
            else:
                if (stack.top() in self.pTable) == False:
                    print "Current Token:", str(token)
                    print "Stack Top:", str(JP.JBParserToken(stack.top()))

                    raise KeyError("{}".format(str(stack.top())))

                if (token.code in self.pTable[stack.top()]) == False:
                    print "Current Token:", str(token)
                    print "Stack Top:", str(JP.JBParserToken(stack.top()))

                    raise KeyError(str(token))

                rule = self.pTable[stack.top()][token.code]

                # Epsilon rule
                if rule == -1:
                    stack.pop()
                elif rule == 1:			# parser -> optional_section definitions_section rules_section grammar_section
                    stack.pop()
                    stack.push(JP.JBParserToken.grammar_section)
                    stack.push(JP.JBParserToken.rules_section)
                    stack.push(JP.JBParserToken.definitions_section)
                    stack.push(JP.JBParserToken.optional_section)
                elif rule == 2:			# optional_section -> L_BRACKET optional_args R_BRACKET
                    stack.pop()
                    stack.push(JP.JBParserToken.R_BRACKET)
                    stack.push(JP.JBParserToken.optional_args)
                    stack.push(JP.JBParserToken.L_BRACKET)
                elif rule == 3:			# optional_args -> optional_arg STRING optional_args_more
                    stack.pop()
                    stack.push(JP.JBParserToken.optional_args_more)
                    stack.push(JP.JBParserToken.STRING)
                    stack.push(JP.JBParserToken.optional_arg)
                elif rule == 4:			# optional_args_more -> COMMA optional_arg STRING optional_args_more
                    stack.pop()
                    stack.push(JP.JBParserToken.optional_args_more)
                    stack.push(JP.JBParserToken.STRING)
                    stack.push(JP.JBParserToken.optional_arg)
                    stack.push(JP.JBParserToken.COMMA)
                elif rule == 5:			# optional_arg -> OPT_PARSER_NAME
                    stack.pop()
                    stack.push(JP.JBParserToken.OPT_PARSER_NAME)
                elif rule == 6:			# optional_arg -> OPT_OUTPUT_LANGUAGE
                    stack.pop()
                    stack.push(JP.JBParserToken.OPT_OUTPUT_LANGUAGE)
                elif rule == 7:			# definitions_section -> DEFINITIONS_HEADER d_statements
                    stack.pop()
                    stack.push(JP.JBParserToken.d_statements)
                    stack.push(JP.JBParserToken.DEFINITIONS_HEADER)
                elif rule == 8:			# d_statements -> d_stmt d_statements
                    stack.pop()
                    stack.push(JP.JBParserToken.d_statements)
                    stack.push(JP.JBParserToken.d_stmt)
                elif rule == 9:			# d_stmt -> STRING PATTERN END_STATEMENT
                    stack.pop()
                    stack.push(JP.JBParserToken.END_STATEMENT)
                    stack.push(JP.JBParserToken.PATTERN)
                    stack.push(JP.JBParserToken.STRING)
                elif rule == 10:			# rules_section -> RULES_HEADER r_statements
                    stack.pop()
                    stack.push(JP.JBParserToken.r_statements)
                    stack.push(JP.JBParserToken.RULES_HEADER)
                elif rule == 11:			# r_statements -> r_stmt r_statements
                    stack.pop()
                    stack.push(JP.JBParserToken.r_statements)
                    stack.push(JP.JBParserToken.r_stmt)
                elif rule == 12:			# r_stmt -> r_start_conditions PATTERN r_actions END_STATEMENT
                    stack.pop()
                    stack.push(JP.JBParserToken.END_STATEMENT)
                    stack.push(JP.JBParserToken.r_actions)
                    stack.push(JP.JBParserToken.PATTERN)
                    stack.push(JP.JBParserToken.r_start_conditions)
                elif rule == 13:			# r_start_conditions -> LESS STRING r_start_conditions_more GREATER
                    stack.pop()
                    stack.push(JP.JBParserToken.GREATER)
                    stack.push(JP.JBParserToken.r_start_conditions_more)
                    stack.push(JP.JBParserToken.STRING)
                    stack.push(JP.JBParserToken.LESS)
                elif rule == 14:			# r_start_conditions_more -> COMMA STRING r_start_conditions_more
                    stack.pop()
                    stack.push(JP.JBParserToken.r_start_conditions_more)
                    stack.push(JP.JBParserToken.STRING)
                    stack.push(JP.JBParserToken.COMMA)
                elif rule == 15:			# r_actions -> r_action r_action_more
                    stack.pop()
                    stack.push(JP.JBParserToken.r_action_more)
                    stack.push(JP.JBParserToken.r_action)
                elif rule == 16:			# r_action_more -> COMMA r_action
                    stack.pop()
                    stack.push(JP.JBParserToken.r_action)
                    stack.push(JP.JBParserToken.COMMA)
                elif rule == 17:			# r_action -> BEGIN LP STRING RP
                    stack.pop()
                    stack.push(JP.JBParserToken.RP)
                    stack.push(JP.JBParserToken.STRING)
                    stack.push(JP.JBParserToken.LP)
                    stack.push(JP.JBParserToken.BEGIN)
                elif rule == 18:			# r_action -> TOKEN LP STRING r_token_more RP
                    stack.pop()
                    stack.push(JP.JBParserToken.RP)
                    stack.push(JP.JBParserToken.r_token_more)
                    stack.push(JP.JBParserToken.STRING)
                    stack.push(JP.JBParserToken.LP)
                    stack.push(JP.JBParserToken.TOKEN)
                elif rule == 19:			# r_token_more -> COMMA TOKEN_ACTION
                    stack.pop()
                    stack.push(JP.JBParserToken.TOKEN_ACTION)
                    stack.push(JP.JBParserToken.COMMA)
                elif rule == 20:			# grammar_section -> GRAMMAR_HEADER g_productions
                    stack.pop()
                    stack.push(JP.JBParserToken.g_productions)
                    stack.push(JP.JBParserToken.GRAMMAR_HEADER)
                elif rule == 21:			# g_productions -> g_production g_productions
                    stack.pop()
                    stack.push(JP.JBParserToken.g_productions)
                    stack.push(JP.JBParserToken.g_production)
                elif rule == 22:			# g_production -> STRING COLON g_right SEMI_COLON
                    stack.pop()
                    stack.push(JP.JBParserToken.SEMI_COLON)
                    stack.push(JP.JBParserToken.g_right)
                    stack.push(JP.JBParserToken.COLON)
                    stack.push(JP.JBParserToken.STRING)
                elif rule == 23:			# g_right -> g_strings g_right_more
                    stack.pop()
                    stack.push(JP.JBParserToken.g_right_more)
                    stack.push(JP.JBParserToken.g_strings)
                elif rule == 24:			# g_right_more -> PIPE g_strings g_right_more
                    stack.pop()
                    stack.push(JP.JBParserToken.g_right_more)
                    stack.push(JP.JBParserToken.g_strings)
                    stack.push(JP.JBParserToken.PIPE)
                elif rule == 25:			# g_strings -> STRING g_strings_more
                    stack.pop()
                    stack.push(JP.JBParserToken.g_strings_more)
                    stack.push(JP.JBParserToken.STRING)
                elif rule == 26:			# g_strings_more -> STRING g_strings_more
                    stack.pop()
                    stack.push(JP.JBParserToken.g_strings_more)
                    stack.push(JP.JBParserToken.STRING)
                else:
                    raise Exception("Rule not recognized.")

        print 'Parsing completed.'

if __name__ == '__main__':
    main()
