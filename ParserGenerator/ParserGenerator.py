import JamesLexer as JL
import JamesGrammar as JG
import pprint
import re

pp = pprint.PrettyPrinter(indent=4)

def main():
    # Lexer definition
    SC_DEFAULT = 0
    SC_GRAMMAR_SECTION = 1

    # Define lexer
    lexer = JL.JBLexer(SC_DEFAULT)

    # Lexer definitions
    lexer.addDefinition("word", "[a-zA-Z]+")
    lexer.addDefinition("string", "[a-zA-Z]\w+")

    # Optional section
    lexer.addRule("OPT_PARSER_NAME", "[Nn][Aa][Mm][Ee]:\s*", JBParserToken.OPT_PARSER_NAME)
    lexer.addRule("OPT_OUTPUT_LANGUAGE", "[Ll][Aa][Nn][Gg]([Uu][Aa][Gg][Ee])?:", JBParserToken.OPT_PARSER_NAME)

    # Sections
    lexer.addRule("DEFINITIONS_HEADER", "\s*[Dd][Ee][Ff][Ii][Nn][Ii][Tt][Ii][Oo][Nn][Ss]:\s+", JBParserToken.DEFINITIONS_HEADER)
    lexer.addRule("RULES_HEADER", "[Rr][Uu][Ll][Ee][Ss]:\s+", JBParserToken.RULES_HEADER)
    lexer.addRule("GRAMMAR_HEADER", "[Gg][Rr][Aa][Mm][Mm][Aa][Rr]:\s+", JBParserToken.GRAMMAR_HEADER)

    lexer.addRule("PROD_ACTION_BODY", "%\{", JBParserToken.PROD_ACTION_BEGIN)

    lexer.addRule("PIPE", "\|\s*", JBParserToken.PIPE)
    lexer.addRule("L_BRACKET", "\[", JBParserToken.L_BRACKET)
    lexer.addRule("R_BRACKET", "\]", JBParserToken.R_BRACKET)
    lexer.addRule("LP", "\(", JBParserToken.LP)
    lexer.addRule("RP", "\)", JBParserToken.RP)
    lexer.addRule("LESS", "<", JBParserToken.LESS)
    lexer.addRule("GREATER", ">", JBParserToken.GREATER)
    lexer.addRule("COMMA", ",", JBParserToken.COMMA)
    lexer.addRule("COLON", ":", JBParserToken.COLON)
    lexer.addRule("EPSILON", "EPSILON", JBParserToken.EPSILON)
    lexer.addRule("BEGIN", "BEGIN", JBParserToken.BEGIN)
    lexer.addRule("TOKEN", "TOKEN", JBParserToken.TOKEN)
    lexer.addRule("TOKEN_ACTION", "IGNORE|SAVE", JBParserToken.TOKEN_ACTION, action=JL.JBLexerAction(JL.JBLexerAction.ActionSaveText))
    lexer.addRule("STRING", "\w+", JBParserToken.STRING, action=JL.JBLexerAction(JL.JBLexerAction.ActionSaveText))
    lexer.addRule("PATTERN", "\"[^\"]+\"", JBParserToken.PATTERN, action=JL.JBLexerAction(JL.JBLexerAction.ActionSaveSubText, [1, -2]))
    lexer.addRule("END_STATEMENT", "[\r\n]+\s*", JBParserToken.END_STATEMENT)
    lexer.addRule("SEMI_COLON", ";\s*", JBParserToken.SEMI_COLON)

    # Ignore whitespace
    lexer.addRule("WHITESPACE", "\s+", -1, action=JL.JBLexerAction(JL.JBLexerAction.ActionIgnoreToken))

    grammar = JG.JBGrammar(lexer, [
        'parser : optional_section definitions_section rules_section grammar_section',
        'optional_section : L_BRACKET optional_args R_BRACKET | EPSILON',
        'optional_args : optional_arg STRING optional_args_more | EPSILON',
        'optional_args_more : COMMA optional_arg STRING optional_args_more | EPSILON',
        'optional_arg : OPT_PARSER_NAME | OPT_OUTPUT_LANGUAGE',
        'definitions_section : DEFINITIONS_HEADER d_statements',
        'd_statements : d_stmt d_statements | EPSILON',
        'd_stmt : STRING PATTERN END_STATEMENT',
        'rules_section : RULES_HEADER r_statements',
        'r_statements : r_stmt r_statements | EPSILON',
        'r_stmt : r_start_conditions PATTERN r_actions END_STATEMENT',
        'r_start_conditions : LESS STRING r_start_conditions_more GREATER | EPSILON',
        'r_start_conditions_more : COMMA STRING r_start_conditions_more | EPSILON',
        'r_actions : r_action r_action_more',
        'r_action_more : COMMA r_action | EPSILON',
        'r_action : BEGIN LP STRING RP | TOKEN LP STRING r_token_more RP',
        'r_token_more : COMMA TOKEN_ACTION | EPSILON',
        'grammar_section : GRAMMAR_HEADER g_productions',
        'g_productions : g_production g_productions | EPSILON',
        'g_production : STRING COLON g_right SEMI_COLON',
        'g_right : g_strings PROD_ACTION_BODY g_right_more',
        'g_right_more : PIPE g_strings PROD_ACTION_BODY g_right_more | EPSILON',
        'g_strings : STRING g_strings_more',
        'g_strings_more : STRING g_strings_more | EPSILON'
        ])

    if grammar.isValid():
        print "Grammar is valid."
    else:
        print "Grammar is NOT valid!"

    pg = ParserGenerator(lexer, grammar)
    pg.generateGrammarCode()


class Language:
    Python = 0

class Utils(object):
    tabSpaces = 4

    """docstring for Utils"""
    def __init__(self):
        super(Utils, self).__init__()
    
    @classmethod
    def expandBlueprint(cls, blueprint, table):
        finalBlueprint = blueprint

        for k in table.keys():
            m = re.findall("(\{" + k + "):(\d+)\}", finalBlueprint)

            if len(m) > 0:
                # Add tabSpaces to lines
                string = "\n".join([(' ' * Utils.tabSpaces * int(m[0][1])) + line for line in re.split("[\r\n]", table[k])])
                string = re.sub("^\s+", "", string)
    
                finalBlueprint = re.sub("\{" + k + ":\d+\}", string, finalBlueprint)

        return finalBlueprint

class JBParserToken(object):
    JB_EOS = 0
    LESS = 1
    PATTERN = 2
    R_BRACKET = 3
    OPT_PARSER_NAME = 4
    GRAMMAR_HEADER = 5
    SEMI_COLON = 6
    STRING = 7
    TOKEN_ACTION = 8
    BEGIN = 9
    RP = 10
    OPT_OUTPUT_LANGUAGE = 11
    PIPE = 12
    COLON = 13
    LP = 14
    PROD_ACTION_BEGIN = 15
    L_BRACKET = 16
    END_STATEMENT = 17
    PROD_ACTION_END = 18
    TOKEN = 19
    DEFINITIONS_HEADER = 20
    WHITESPACE = 21
    GREATER = 22
    EPSILON = 23
    RULES_HEADER = 24
    COMMA = 25
    parser = 26
    optional_section = 27
    optional_args = 28
    optional_args_more = 29
    optional_arg = 30
    definitions_section = 31
    d_statements = 32
    d_stmt = 33
    rules_section = 34
    r_statements = 35
    r_stmt = 36
    r_start_conditions = 37
    r_start_conditions_more = 38
    r_actions = 39
    r_action_more = 40
    r_action = 41
    r_token_more = 42
    grammar_section = 43
    g_productions = 44
    g_production = 45
    g_right = 46
    g_right_more = 47
    g_strings = 48
    g_strings_more = 49

    def __init__(self, code, var=None):
        super(JBToken, self).__init__()
        self.code = code
        self.var = var

class JBStack: 
    def __init__(self, items=None):
        self.items = items

        if self.items == None:
            self.items = [] 

    def top(self):
        return self.items[-1]

    def push(self, item):
        self.items.append(item) 

    def pop(self):
        return self.items.pop() 

    def isEmpty(self):
        return (self.items == [])

    def isNotEmpty(self):
        return (self.items != [])

    def __repr__(self):
        return "JBStack({})".format(self.items)

class ParserGenerator(object):
    def __init__(self, lexer, grammar, language=Language.Python):
        super(ParserGenerator, self).__init__()
        self.lexer = lexer
        self.grammar = grammar
        self.language = language

    def generateLexerCode(self):
        pass

    def generateGrammarCode(self):
        if self.language == Language.Python:
            return self.pythonGenerateGrammarCode()
        else:
            raise Exception("Not implemented!")

    def pythonGenerateGrammarCode(self):
        pythonUserParserBlueprint = open('Blueprints/Python/UserParser.jbb', 'r').read()
        pythonJamesParserBlueprint = open('Blueprints/Python/JamesParser.jbb', 'r').read()

        table = {}

        # Placeholder: parser_name
        table['parser_name'] = "UserParser"

        # Placeholder: list_of_tokens 
        nonTerminals = self.grammar.getNonTerminals()
        terminals = self.grammar.getTerminals()

        list_of_tokens = ['JB_EOS'] + list(terminals) + list(nonTerminals)

        table['list_of_tokens'] = "\n".join(["{} = {}".format(t, i) for (t, i) in zip(list_of_tokens, range(0, len(list_of_tokens)))])

        # Placeholder: string_code_value

        table['string_code_value'] = ",\n".join(["{}: '{}'".format(i, t) for (t, i) in zip(list_of_tokens, range(0, len(list_of_tokens)))])

        # Placeholder: starting_token
        table['starting_token'] = self.grammar.getFirstNonTerminal()

        # Placeholder: table_initialization
        parsingTable = self.grammar.getParsingTable()

        table_lines = []
        for k in parsingTable.keys():
            line = ", ".join(["JBParserToken.{}: {}".format(j, parsingTable[k][j]) for j in parsingTable[k].keys()])

            table_lines.append("table[JBParserToken.{}] = {{{}}}".format(k, line))

        table['table_initialization'] = "\n".join(table_lines)

        # Placeholder: token_non_match_code
        token_non_match_code = []

        listWithLineTokens = self.grammar.getListWithLineTokens()
        for lineNum in sorted(listWithLineTokens.keys()):
            pName = listWithLineTokens[lineNum][0]
            tokens = listWithLineTokens[lineNum][1]

            block = ["elif rule == {}:\t\t\t# {} -> {}\n{}stack.pop()".format(lineNum, pName, " ".join(tokens), Utils.tabSpaces * ' ')]

            for t in reversed(tokens):
                block.append("{}stack.push(JP.JBParserToken.{})".format(Utils.tabSpaces * ' ', t))

            token_non_match_code.append("\n".join(block))

        table['token_non_match_code'] = "\n".join(token_non_match_code)

        table['lexer_start_condition'] = str(self.lexer.defaultStartCondition)

        table['lexer_rules'] = self.pythonGenerateLexerRules(self.lexer)

        finalPythonJamesParser = Utils.expandBlueprint(pythonJamesParserBlueprint, table)
        finalPythonUserParser = Utils.expandBlueprint(pythonUserParserBlueprint, table)

        # return final

        open('JamesParser.py', 'w').write(finalPythonJamesParser)
        open(table['parser_name'] + '.py', 'w').write(finalPythonUserParser)

    def pythonGenerateLexerRules(self, lexer):
        string = []

        ruleNumber = 1
        string.append("rules = []\n\n")
        for rule in lexer.rules:
            stack = JBStack()
            stack.push(rule.nfa)

            visitedStates = {}

            while stack.isNotEmpty():
                currState = stack.pop()

                visitedStates[currState.code] = currState

                for t in currState.transitions:
                    if (t.nextState.code in visitedStates) == False:
                        stack.push(t.nextState)

            # Declare states
            for k in sorted(visitedStates.keys()):
                s = visitedStates[k]

                string.append("s_{:04} = JBLexerState({}, None, {}, {}, {})\n".format(k, k, s.isStartState, s.isFinalState, s.inputIndex))

            string.append("\n")

            startState = None

            # Initialize states' transitions
            for k in sorted(visitedStates.keys()):    
                s = visitedStates[k]

                if startState == None and s.isStartState == True:
                    startState = s

                transBlock = []
                for t in s.transitions:
                    if t.tokenMatch.var != None:
                        tokenVar = t.tokenMatch.var
                        tokenVar = repr(tokenVar)[1:-1]
                        tokenVar = re.sub(r"\\n", r"\\\\n", tokenVar)
                        tokenVar = re.sub(r"\\r", r"\\\\r", tokenVar)
                        tokenVar = re.sub(r"\\t", r"\\\\t", tokenVar)
                        tokenVar = "'" + tokenVar + "'"
                    else:
                        tokenVar = None

                    transBlock.append(r"JBLexerTransition(JBLexerToken({}, {}), s_{:04})". format(t.tokenMatch.code, 
                        tokenVar, t.nextState.code))

                string.append("s_{:04}.transitions = [".format(k) + ",\n\t".join(transBlock) + "]\n")

            if startState == None:
                raise Exception("FFS!!! NO START STATE!!!!!")

            string.append("\n")

            # Declare rule
            pattern = rule.pattern
            pattern = repr(pattern)[1:-1]
            pattern = re.sub(r"\\n", r"\\\\n", pattern)
            pattern = re.sub(r"\\r", r"\\\\r", pattern)
            pattern = re.sub(r"\\t", r"\\\\t", pattern)
            string.append("rules.append(JBLexerRule('{}', '{}', {}, s_{:04}, {}, {}, {}))\n\n".format(rule.name, pattern, 
                rule.tokenCode, startState.code, repr(rule.startConditions), rule.setCondition, rule.action))

            ruleNumber += 1

        string.append("self.rules = rules")

        return "".join(string)

if __name__ == '__main__':
    main()