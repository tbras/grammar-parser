# -*- coding: utf-8 -*-

import re
import JamesGrammarTests as JGT
import JamesLexer as JL
import pprint as pp

def main():
    JGT.runAllTests()

# TODO: substituir usando regex, {...} por JBGrammarToken (fazer copia de seguranca)

class JBGrammarToken(object):
    def __init__(self, name, code, pNum):
        super(JBGrammarToken, self).__init__()
        self.name = name
        self.code = code
        self.pNum = pNum

    def __repr__(self):
        return "JBGrammarToken({}, {}, {})".format(self.name, self.code, self.pNum)

    def __str__(self):
        return "({}, {}, {})".format(self.name, self.code, self.pNum)


class JBGrammar(object):
    P_NAME, P_NUM, P_LINES, P_TOKENS, P_ISNULLABLE = ('NAME', 'NUM', 'LINES', 'TOKENS', 'ISNULLABLE')

    """
    productions = {
        'non_terminal': [
            {'TOKENS': ['terminal', ...], 'NUM': 1}, ...
        ],
        ...
    }
    """
    def __init__(self, lexer, grammarDefinition):
        super(JBGrammar, self).__init__()

        if isinstance(lexer, JL.JBLexer) == False:
            raise TypeError("'def __init__(self, lexer)': 'lexer' must be of type 'JBLexer'")

        self.productionsCounter = 1

        self.terminalsCodes = lexer.getLexerTokensDict()
        self.isNullableDict = {}

        # All terminals are not nullable
        for t in self.terminalsCodes:
            self.isNullableDict[t] = False

        # Format: {'non_terminal': [{'TOKENS': ['token', ...], 'P_NUM': 1}, ...], ...}
        self.productions = {}
        self.orderedProductions = []

        if len(grammarDefinition) < 1:
            raise Exception("Grammar definition must have at least one production.")

        self.firstNonTerminal = None

        # Add productions
        for d in grammarDefinition:
            self.addProduction(d)

        self.firstSets = self.calculateFirstSets()
        self.followSets = self.calculateFollowSets()
        self.parsingTable = self.genParsingTable()

        # TODO: make sure all non terminals have productions, and all terminals have rules

    # Public function
    def getFirstSets(self):
        return self.firstSets

    # Public function
    def getFollowSets(self):
        return self.followSets

    # Public function
    def getParsingTable(self):
        return self.parsingTable

    # Public function
    def getTerminals(self):
        return self.terminalsCodes.keys()

    # Public function
    def getNonTerminals(self):
        return self.orderedProductions

    # Public function
    def getFirstNonTerminal(self):
        return self.firstNonTerminal

    # Public function
    def getParsingTable(self):
        return self.parsingTable

    # Public function
    def getListWithLineTokens(self):
        table = {}
        for p in self.productions:
            for line in self.productions[p]:
                table[line[JBGrammar.P_NUM]] = [p, line[JBGrammar.P_TOKENS]]

        return table


    # Private function
    def terminalCode(self, token):
        if type(token) != str:
            raise TypeError("'def terminalCode(self, token)': 'token' must be of type 'str'")

        return self.terminalsCodes[token]

    # Private function
    def isTerminal(self, token):
        if type(token) != str:
            raise TypeError("'def isTerminal(self, token)': 'token' must be of type 'str'")

        return (token in self.terminalsCodes)

    # Private function
    def isNullable(self, token):
        if type(token) != str:
            raise TypeError("'def isNullable(self, token)': 'token' must be of type 'str'")

        return self.isNullableDict[token]

    # Private function
    def tokensForProductionNumber(self, pNum):
        if type(token) != int:
            raise TypeError("'def tokensForProductionNumber(self, token)': 'token' must be of type 'int'")

        for p in self.productions:
            for line in p:
                if line[JBGrammar.P_NUM] == pNum:
                    return line[JBGrammar.P_TOKENS]

        raise Exception("There isn't any production with number '{}'.".format(pNum))

    # Private function
    def nonTerminalForProductionNumber(self, pNum):
        if type(token) != int:
            raise TypeError("'def tokensForProductionNumber(self, token)': 'token' must be of type 'int'")

        for nonTerminal in self.productions.keys():
            for line in self.productions[nonTerminal]:
                if line[JBGrammar.P_NUM] == pNum:
                    return nonTerminal

        raise Exception("There isn't any production with number '{}'.".format(pNum))

    # Private function
    def addProduction(self, definition):
        if isinstance(definition, str) == False:
            raise TypeError("'addProduction(self, definition)': 'definition' must be of type 'str'")

        rightHandSide = None
        leftHandSide = None
        linesWithoutEpsilon = None

        d = re.sub('\s+', ' ', definition)

        validationPattern = re.compile("\w+ : (\s*\w+\s*)(\|\s*\w+\s*)*")

        # Validate definition
        if validationPattern.match(re.sub('\s+', ' ', d)) == None:
            raise Exception("Production '{}' syntax is not valid. Check spaces between words and symbols.".format(definition))

        leftHandSide, rightHandSide = [side.strip() for side in d.split(':')]

        self.orderedProductions.append(leftHandSide)

        # First production
        if self.firstNonTerminal == None:
            self.firstNonTerminal = leftHandSide

        lines = [line.strip() for line in rightHandSide.split('|')]

        isNullable = 'EPSILON' in lines

        linesWithoutEpsilon = [line for line in lines if (line != 'EPSILON')]

        # # # # #

        self.isNullableDict[leftHandSide] = isNullable

        self.productions[leftHandSide] = []

        # For each production line, generate grammar tokens
        for line in linesWithoutEpsilon:
            lineTokens = []

            for t in [token.strip() for token in line.split()]:
                lineTokens.append(t)

            self.productions[leftHandSide].append(
                {JBGrammar.P_NUM: self.productionsCounter, JBGrammar.P_TOKENS: lineTokens})

            self.productionsCounter += 1

    # Private function
    def calculateFirstSets(self):
        # = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
        # Helper function to add elements without repeating
        def addElement(theList, grammarToken):
            if isinstance(grammarToken, JBGrammarToken) == False:
                raise TypeError("'addProduction(self, grammarToken)': 'grammarToken' must be of type 'JBGrammarToken'")

            exists = False

            for el in theList:
                if el.pNum == grammarToken.pNum and \
                    el.name == grammarToken.name:
                    exists = True
                    break

            if exists == False:
                theList.append(grammarToken)
        # = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

        firstSetsTokens = {}

        nonTerminals = {}

        # 1st pass: adicionar todos os terminais
        for pName in self.productions.keys():
            pLines = self.productions[pName]

            firstSetsTokens[pName] = []
            nonTerminals[pName] = []

            for line in pLines:
                pNum = line[JBGrammar.P_NUM]
                pTokens = line[JBGrammar.P_TOKENS]

                lastNonTerminal = None
                for t in pTokens:
                    if self.isTerminal(t):
                        firstSetsTokens[pName].append(JBGrammarToken(t, self.terminalsCodes[t], pNum))

                        break
                    else:
                        nonTerminals[pName].append(JBGrammarToken(t, None, pNum))

                        lastNonTerminal = t

                        if self.isNullable(t) == False:
                            break

                if lastNonTerminal != None and self.isNullable(lastNonTerminal):
                    addElement(firstSetsTokens[pName], JBGrammarToken('EPSILON', self.terminalsCodes['EPSILON'], -1))


        # 2nd pass: adicionar os terminais correspondentes a cada non-terminal
        for i in range(len(firstSetsTokens)):
            # Format: {'str': [{P_NAME:'str', P_NUM:int}, ...], ...}
            for pNameKey in nonTerminals.keys():
                # Format: [{P_NAME:'str', P_NUM:int}, ...]
                for nonTerminal in nonTerminals[pNameKey]:
                    for t in firstSetsTokens[nonTerminal.name]:
                        if t.name != 'EPSILON':
                            mod_t = JBGrammarToken(t.name, self.terminalsCodes[t.name], nonTerminal.pNum)

                            addElement(firstSetsTokens[pNameKey], mod_t)

        # 3rd pass: adicionar epsilons
        for nonTerminal in firstSetsTokens.keys():
            if self.isNullable(nonTerminal):
                firstSetsTokens[nonTerminal].append(JBGrammarToken('EPSILON', self.terminalsCodes['EPSILON'], -1))

        return firstSetsTokens
    
    # Private function        
    def calculateFollowSets(self):
        if self.firstSets == None:
            raise Exception("First sets weren't set yet.")

        # = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
        # Helper function to add elements without repeating
        def addElement(theList, grammarToken):
            if isinstance(grammarToken, JBGrammarToken) == False:
                raise TypeError("'addProduction(self, grammarToken)': 'grammarToken' must be of type 'JBGrammarToken'")

            exists = False

            for el in theList:
                if el.pNum == grammarToken.pNum and \
                    el.name == grammarToken.name:
                    exists = True
                    break

            if exists == False:
                theList.append(grammarToken)
        # = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

        nonTerminals = self.productions.keys()
        followSetsTokens = {}
        followsAddLater = {}
        
        for nonTerminal in self.productions.keys():
            followSetsTokens[nonTerminal] = []
            followsAddLater[nonTerminal] = []

            for pName in self.productions.keys():
                pLines = self.productions[pName]
    
                for line in pLines:
                    pNum = line[JBGrammar.P_NUM]
                    pTokens = line[JBGrammar.P_TOKENS]
                    
                    tokensLen = len(pTokens)
                    for i in range(tokensLen):
                        t = pTokens[i]

                        if t == nonTerminal:
                            # If it's NOT last token
                            if i < tokensLen - 1:
                                nextToken = pTokens[i+1]

                                # Rule: A -> aBb, Follow(a) += {B}
                                if self.isTerminal(nextToken):
                                    addElement(followSetsTokens[nonTerminal], JBGrammarToken(nextToken, self.terminalsCodes[nextToken], -1))
                                else:
                                    # Rule: A -> aBb, Follow(B) += First(b)
                                    for fToken in self.firstSets[nextToken]:
                                        if fToken.name != 'EPSILON':
                                            addElement(followSetsTokens[nonTerminal], JBGrammarToken(fToken.name, self.terminalsCodes[fToken.name], -1))

                                    # Rule: A -> aBb, b is Nullable, Follow(B) += Follow(A)  
                                    if (i + 1 == tokensLen - 1) and self.isNullable(nextToken):
                                        followsAddLater[nonTerminal].append(pName)

                            # If it's last token
                            elif i == tokensLen - 1:
                                # Rule: A -> aBb, Follow(b) += Follow(A)
                                if (pName in followsAddLater[nonTerminal]) == False:
                                    followsAddLater[nonTerminal].append(pName)

        # pp.pprint(followsAddLater)
        for i in range(len(followSetsTokens)):
            for nonTerminal in followsAddLater.keys():
                for t in followsAddLater[nonTerminal]:
                    for fToken in followSetsTokens[t]:
                        addElement(followSetsTokens[nonTerminal], fToken)

        return followSetsTokens

    # Public function 
    def isValid(self):
        ambiguity = False

        # Look for ambiguity
        for fToken in self.firstSets.keys():
            names = [t.name for t in self.firstSets[fToken] if t.name != 'EPSILON']
            
            if len(names) != len(set(names)):
                print "Production '{}' ambiguous.".format(fToken)

                ambiguity = True

        for k in self.firstSets.keys():
            firstSetNames = [t.name for t in self.firstSets[k] if t.name != 'EPSILON']
            followSetNames = [t.name for t in self.followSets[k] if t.name != 'EPSILON']

            if self.isNullable(k):
                for n in firstSetNames:
                    if (n in followSetNames):
                        print "Not valid. Token '{}' is in first set of '{} : {}' and in follow set '{} : {}'.".format(n, k, firstSetNames, k, followSetNames)

                        ambiguity = True

        if ambiguity:
            return False
        else:
            return True

    # Private function
    def genParsingTable(self):
        if self.firstSets == None:
            raise Exception("First sets aren't set yet.")

        if self.followSets == None:
            raise Exception("Follow sets aren't set yet.")

        table = {}

        for p in self.orderedProductions:
            table[p] = {}
            for t in self.firstSets[p]:
                if t.name != 'EPSILON':
                    if t.name in table[p]:
                        raise Exception("Ambiguity found in production '{}'. '{}' row already defined.".format(p, t.name))

                    table[p][t.name] = t.pNum

            if self.isNullable(p):
                for t in self.followSets[p]:
                    if t.name in table[p]:
                        raise Exception("Ambiguity found in production '{}'. '{}' row already defined.".format(p, t.name))

                    table[p][t.name] = t.pNum

        return table



        

if __name__ == '__main__':
    main()
        